package com.liver_cloud.bean;

public class PatientDoctor {
    private Integer dpId;

    private Integer dpDoctorId;

    private Integer dpPatientId;

    private Byte mDeletePatient;

    public Integer getDpId() {
        return dpId;
    }

    public void setDpId(Integer dpId) {
        this.dpId = dpId;
    }

    public Integer getDpDoctorId() {
        return dpDoctorId;
    }

    public void setDpDoctorId(Integer dpDoctorId) {
        this.dpDoctorId = dpDoctorId;
    }

    public Integer getDpPatientId() {
        return dpPatientId;
    }

    public void setDpPatientId(Integer dpPatientId) {
        this.dpPatientId = dpPatientId;
    }

    public Byte getmDeletePatient() {
        return mDeletePatient;
    }

    public void setmDeletePatient(Byte mDeletePatient) {
        this.mDeletePatient = mDeletePatient;
    }
}