package com.liver_cloud.dao;

import com.liver_cloud.bean.PatientDoctor;

public interface PatientDoctorMapper {
    int deleteByPrimaryKey(Integer dpId);

    int insert(PatientDoctor record);

    int insertSelective(PatientDoctor record);

    PatientDoctor selectByPrimaryKey(Integer dpId);

    int updateByPrimaryKeySelective(PatientDoctor record);

    int updateByPrimaryKey(PatientDoctor record);
}