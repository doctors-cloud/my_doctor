//请求
(function () {
    var href = location.href;
    var params = href.split('?');
    var userId = params[1].split('=')[1];
    $.ajax({
        url: baseUrl + '/patient/getCaloryOfWeek.do',
        type: 'GET', //GET
        async: true, //或false,是否异步
        data: {
            'userId': userId
        },
        timeout: 5000, //超时时间
        dataType: 'json', //返回的数据格式：json/xml/html/script/jsonp/text
        beforeSend: function (xhr) {
            document.querySelector('.load-container').style.display = 'block';
            console.log(xhr)
            console.log('发送前')
        },
        success: function (data, textStatus, jqXHR) {
            document.querySelector('.load-container').style.display = 'none';
            if (data.values) {
                var qArr = data.values,
                    qList = document.querySelector('.q-list');
                for (var i = 0; i < qArr.length; i++) {
                    var element = qArr[i];
                    var oLi = document.createElement('li'),
                        oSpanL = document.createElement('span'),
                        oSpanR = document.createElement('span');
                    oLi.setAttribute('class', 'clear');
                    oSpanL.setAttribute('class', 'fl');
                    oSpanL.innerHTML = element.calory + '千卡';
                    oSpanR.setAttribute('class', 'fr');
                    oSpanR.innerHTML = transition(element.date);
                    oLi.appendChild(oSpanL);
                    oLi.appendChild(oSpanR);
                    qList.appendChild(oLi);
                }
            }
        },
        error: function (xhr, textStatus) {
            document.querySelector('.load-container').style.display = 'none';
        },
        complete: function () {
            document.querySelector('.load-container').style.display = 'none';
        }
    })
}())