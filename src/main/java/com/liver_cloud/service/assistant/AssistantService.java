package com.liver_cloud.service.assistant;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.liver_cloud.dao.mybatis.DaoSupport;
import com.liver_cloud.util.PageData;

@Service
public class AssistantService {

	@Resource(name = "daoSupport")
	private DaoSupport dao;
	
	//助手注册，填写基本信息
	public PageData saveAssistantInfo(PageData pd) throws Exception {
		//根据userId查询是否已经有信息，如果有直接update，并将ifrecord改为1.如果没有，则直接插入,并将ifrecord改为1
		String diagnosisType = pd.getString("diagnosisType");
		String assistantId = 
				(String) dao.findForObject("AssistantMapper.findAssistantId", pd);
		PageData result = new PageData();
		if(assistantId==null||"".equals(assistantId)){
			//直接插入信息，并返回插入的数据id
			dao.save("AssistantMapper.saveAssistantInfo", pd);
			//插入助手擅长病症
			saveSkill(pd,diagnosisType);
			result.put("assistantId", pd.get("id"));
			return result;
		}else{
			pd.put("assistantId", assistantId);
			pd.put("id", assistantId);
			dao.update("AssistantMapper.updateAssistantInfo", pd);	
			result.put("assistantId", assistantId);
			return result;
		}
	}

	private void saveSkill(PageData pd, String diagnosisType) throws Exception {
		if(diagnosisType!=null&&!"".equals(diagnosisType)){
			String[] diagnosisTypeArray = diagnosisType.split(",");
			for(int i=0;i<diagnosisTypeArray.length;i++){
				pd.put("diagnosisTypeId", diagnosisTypeArray[i]);
				dao.save("AssistantMapper.saveSkill", pd);
			}
		}	
		
	}
	
	//助手获取自己的信息列表
	public PageData getMessageList(PageData pd) {
		//
		
		
		return null;
	}
	
	
	//获取患者列表
	public List<PageData> getPatientList(PageData pd) throws Exception {
		
		//获取和该助手相关的患者，同时包括已经转给其他助手的患者
		List<PageData> patientList = 
				(List<PageData>) dao.findForList("AssistantMapper.getAssistantPatient", pd);
		String doctorId = 
				(String) dao.findForObject("AssistantMapper.getDoctorIdByAssistantId", pd);
		//遍历集合，添加diagnosisNameList，病人标签集合
		for(int i=0;i<patientList.size();i++){
			//根据assistantId，找到对应的doctorId，同时集合patientId，查找对应的自定义标签
			pd.put("doctorId", doctorId);
			pd.put("patientId", patientList.get(i).get("patientId"));
			List<String> diagnosisNameList = 
					(List<String>) dao.findForList("DoctorMapper.getDefinedTitel", pd);
			patientList.get(i).put("diagnosisNameList", diagnosisNameList);
			
		}
		return patientList;
	}
	
	//获取病情诊断，就诊时间，最近一次的复查时间
	public PageData getPatientDetail(PageData pd) throws Exception {
		// 根据patientId查询
		PageData PatientDetail =
				(PageData) dao.findForObject("PatientMapper.getPatientDetail", pd);
		if(PatientDetail == null){
			return null;
		}
		if(PatientDetail.get("createTime")==null){
			PatientDetail.put("createTime", "");
		}
		if(PatientDetail.get("checkTime")==null){
			PatientDetail.put("checkTime", "");
		}
		//添加患者病例诊断
		List<String> diagnosisNameList = 
				(List<String>) dao.findForList("PatientMapper.getDiagnosisNameList", pd);
		PatientDetail.put("diagnosisNameList", diagnosisNameList);
		return PatientDetail;
	}
	
	//按手机号，标签，姓名搜索
	public List<PageData> searchPatient(PageData pd) throws Exception {
		// 
		List<PageData> patientList = 
				(List<PageData>) dao.findForList("AssistantMapper.searchPatient", pd);
		//遍历集合，补充全标签
		for(int i=0;i<patientList.size();i++){
			List<String> diagnosisNameList = 
					(List<String>) dao.findForList("PatientMapper.getDiagnosisNameList", patientList.get(i));
			patientList.get(i).put("diagnosisNameList", diagnosisNameList);
		}		
		return patientList;
	}
	
	
	
}
