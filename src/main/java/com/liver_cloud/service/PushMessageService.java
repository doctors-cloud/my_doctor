package com.liver_cloud.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.liver_cloud.bean.PushMessage;
import com.liver_cloud.dao.PushMessageMapper;
import com.liver_cloud.dao.PushMessageReaderMapper;

@Service
public class PushMessageService {

	@Resource
	private PushMessageMapper pushMessageMapper;
	
	@Resource
	private PushMessageReaderMapper pushMessageReaderMapper;
	
	
	public void test(){
		PushMessage pushMessage = pushMessageMapper.selectByPrimaryKey(1);
		System.out.println(pushMessage);
		
	}
	
	/**
	 * 获取图片列表，设置到PushMessage
	 * @return
	 */
	public void setPushMessageImages(PushMessage pushMessage){
		

	}
	
	/**
	 * 获取读者列表和点赞者列表，设置到PushMessage
	 * @return
	 */
	public void setPushMessageReadersAndSupports(PushMessage pushMessage){
		
	}
	
	public List<PushMessage> getPushMessageListByReaderUserIdAndKeywords(Integer readerUserId,String keyword){
		
		List<Integer> messageIdList = pushMessageReaderMapper.selectByReaderUserId(readerUserId);
		
		Map<String, Object> params = new HashMap<String,Object>();
		params.put("keyword", keyword);
		params.put("ids", messageIdList);
		List<PushMessage> pushMessageList= pushMessageMapper.selectByPrimaryKeyList(params);
		
		System.out.println(pushMessageList);
		return pushMessageList;
	}
	
}
