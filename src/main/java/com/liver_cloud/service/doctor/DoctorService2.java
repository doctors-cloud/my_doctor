package com.liver_cloud.service.doctor;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.liver_cloud.dao.mybatis.DaoSupport;
import com.liver_cloud.entity.Page;
import com.liver_cloud.model.ServiceResult;
import com.liver_cloud.util.Consts;
import com.liver_cloud.util.EnumErrorCode;
import com.liver_cloud.util.PageData;

@Service
public class DoctorService2 {
	
	@Resource(name = "daoSupport")
	private DaoSupport dao;

	/**
	 * 根据医生id和患者id列表删除该医生的若干患者
	 * @param doctorId
	 * @param patientIdArray
	 * @throws Exception
	 */
	public void deletePatient(String doctorId, String[] patientIdArray) throws Exception {
		HashMap<String, String> params = new HashMap<>();
		params.put("doctorId", doctorId);

		for(String patientId:patientIdArray){
			if(patientId!=null&&!patientId.equals("")){
				params.put("patientId", patientId);
				dao.update("DoctorAssistantPatientMapper.doctorDeletePatient", params);
			}
		}

	}

	/**
	 * 根据
	 * @param pd
	 * @throws Exception
	 */
	public void deletePatientTitel(PageData pd) throws Exception {
		//md_doctor_defined_titel表，根据ddt_doctor_id和ddt_patient_titel删除其中一条记录
		dao.delete("Doctor2Mapper.deleteDoctorDefinedTitel", pd);
		//md_patient_title表，根据pt_doctor_id和pt_patient_title_id删除多条记录
		dao.delete("Doctor2Mapper.deletePatientTitel", pd);
	}

	/**
	 * 
	 * @param pd
	 * @return
	 * @throws Exception
	 */
	public PageData addPatientTitel(PageData pd) throws Exception {
		List<String> titles = (List<String>) dao.findForList("Doctor2Mapper.getTitleByName", pd);
		if(titles.size()>0){
			pd.clear();
			pd.put("errorcode", EnumErrorCode.CODE_000302.code);
			pd.put("errormsg", EnumErrorCode.CODE_000302.msg);
			pd.put("result", "error");
			return pd;
		}
		System.out.println(titles.toString());
		dao.save("Doctor2Mapper.savePatientTitel", pd);
		
		return null;
	}

	/**
	 * 
	 * @param pd
	 * @return
	 * @throws Exception
	 */
	public String getPatientNum(PageData pd) throws Exception {
		String patientNum = (String) dao.findForObject("DoctorAssistantPatientMapper.getPatientNum", pd);
		return patientNum;
	}

	/**
	 * 
	 * @param pd
	 * @return
	 * @throws Exception
	 */
	public ServiceResult<Map<String,String>> getPlatformUserByPhone(PageData pd) throws Exception {
		ServiceResult<Map<String,String>> serviceResult = new ServiceResult<>();
		Map<String,String> userAssistantInfo=new HashMap<>();
		serviceResult.setResultValue(userAssistantInfo);
		serviceResult.setResultEnum(EnumErrorCode.CODE_000000);
		PageData userInfo=(PageData) dao.findForObject("UserMapper.getUserInfo", pd);
		//未找到用户
		if(userInfo==null){
			return serviceResult;
		}
		userAssistantInfo.put("nickName", userInfo.getString("u_nick_name"));
		userAssistantInfo.put("account", userInfo.getString("u_mobile"));
		userAssistantInfo.put("imageUrl", userInfo.getString("u_head_portrait"));
		pd.put("userId", userInfo.get("user_id"));
		PageData assistantInfo = (PageData) dao.findForObject("Assistant2Mapper.getAssistantInfo", pd);
		//用户不是助手
		if(assistantInfo==null){
			userAssistantInfo.put("type", Consts.AssistantTypeNo);
			return serviceResult;
		}
		userAssistantInfo.put("nickName", assistantInfo.getString("a_assistant_name"));
		pd.put("assistantId", assistantInfo.get("a_id"));
		PageData doctorAssistantInfo = (PageData) dao.findForObject("DoctorAssistantPatientMapper.getDoctorAssistantInfo", pd);
		
		//助手未绑定医生
		if(doctorAssistantInfo==null){
			
			userAssistantInfo.put("type", Consts.AssistantTypeUnbind);
			return serviceResult;
		}
		if(doctorAssistantInfo.get("ad_doctor_id").equals(pd.get("doctorId"))){
			userAssistantInfo.put("type", Consts.AssistantTypeSelf);
		}else{
			userAssistantInfo.put("type", Consts.AssistantTypeOther);
		}
		return serviceResult;
	}

	/**
	 * 
	 * @param pd
	 * @return
	 * @throws Exception
	 */
	public ServiceResult<List<Map<String,String>>> getPatientTitel(PageData pd) throws Exception {
		ServiceResult<List<Map<String,String>>> serviceResult = new ServiceResult<>();
		List<Map<String,String>> titleListResult=new ArrayList<>();
		serviceResult.setResultMessage("ok");
		serviceResult.setResultEnum(EnumErrorCode.CODE_000000);
		serviceResult.setResultValue(titleListResult);
		//如果是助手
		if("2".equals(pd.getString("pushType"))){
			//获取助手对应医生
			pd.put("assistantId", pd.getString("userId"));
			PageData assistantDoctor  = (PageData) dao.findForObject("DoctorAssistantPatientMapper.getDoctorAssistantInfo", pd);
			//如果没有对应医生，也就没有标签，返回空
			if(assistantDoctor==null){
				return serviceResult;
			}
			pd.put("doctorId", assistantDoctor.get("ad_doctor_id"));
		}else if("1".equals(pd.getString("pushType"))){
			pd.put("doctorId", pd.getString("userId"));
		}else{
			serviceResult.setResultMessage("error");
			serviceResult.setResultEnum(EnumErrorCode.CODE_000303);
			return serviceResult;
		}
		//获取医生的标签列表 ddt_id ddt_patient_titel 
		List<PageData> titleList = (List<PageData>) dao.findForList("Doctor2Mapper.getTitleList", pd);
		Map<String,String> titleInfo;
		for(PageData title:titleList){
			titleInfo=new HashMap<>();
			titleInfo.put("patientTitel", title.getString("ddt_patient_titel"));
			titleInfo.put("titelId", title.get("ddt_id")+"");
			titleListResult.add(titleInfo);
		}
		
		
		return serviceResult;
	}

	/**
	 * 6搜索推送信息接口
	 * ```
		[{ 
		"name" : "提莫",  
		"imageUrl" : "http://wx.qlogo.cn", 
		"createTime" : "时间戳",//信息创建时间,毫秒值
		"pushMessageId" : "12", 
		"readNum" : "48"//阅读人数,
		"profession":"主治医师",
		"supportNum" : "20",//点赞人数，
		"ifChanged":"true",//是否是新消息，或是本人推送的消息有人留言
		"content" : "在我的心上，自由的飞翔", 
		"title" : "恋爱的犀牛" ,
		"imageList":"[xxxx,yyyy]" ,
		"ifSupport":"true"//本人是否点过赞
		}]
		
		```
	 * @return
	 */
//	public ServiceResult<List<Map<String, String>>> getPushInforMation(PageData pd) {
//		ServiceResult<List<Map<String,String>>> serviceResult = new ServiceResult<>();
//		List<Map<String,String>> listResult=new ArrayList<>();
//		serviceResult.setResultMessage("ok");
//		serviceResult.setResultEnum(EnumErrorCode.CODE_000000);
//		serviceResult.setResultValue(listResult);
//		
//		List<PageData> pushMessageList=dao.findForList(str, pd);
//		Map<String,String> pushMessageMap;
//		for(PageData pushMessagePd:pushMessageList){
//			Integer pushId=(Integer) pushMessagePd.get("pm_id");
//			Integer pushUserId=(Integer) pushMessagePd.get("pm_user_id");
//			String pushDate=(String) pushMessagePd.get("pm_date");
//			String pushTitle=(String) pushMessagePd.get("pm_title");
//			String pushContent=(String) pushMessagePd.get("pm_content");
//			String pmChanged=(String) pushMessagePd.get("pm_if_changed");
//			
//			
//			// 从用户表查询用户信息
//			PageData userPd=dao.findForObject(str, pd);
//			
//			pushMessageMap.put("name", userPd.getString("u_nick_name"));
//			pushMessageMap.put("imageUrl", userPd.getString("u_head_portrait"));
//			
//			pushMessageMap.put("createTime", pushDate);
//			pushMessageMap.put("pushMessageId", pushId+"");
//			
//			// 从阅读人表查询阅读人数
//			pd.put("pushMessageId", pushId);
//			String readerNum=dao.findForObject(str, pd);
//			pushMessageMap.put("readNum", readerNum);
//			
//			// 如果是医生
//			if("1".equals(pd.getString("userType"))){
//				// 根据平台id获取医生详情-连接查询
//				PageData doctorPd=dao.findForObject(str, pd);
//				String profession=(String) doctorPd.get("");
//				pushMessageMap.put("", "");
//			}
//			pushMessageMap.put("profession", );
//			pushMessageMap.put("supportNum", );
//			pushMessageMap.put("ifChanged", );
//			pushMessageMap.put("content", );
//			pushMessageMap.put("title", );
//			pushMessageMap.put("imageList", );
//			pushMessageMap.put("ifSupport", );
//		}
//		
//		return serviceResult;
//	}
	
}
