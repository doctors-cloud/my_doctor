package com.liver_cloud.controller.assistant;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.liver_cloud.controller.base.BaseController;
import com.liver_cloud.service.assistant.AssistantService;
import com.liver_cloud.util.EnumErrorCode;
import com.liver_cloud.util.PageData;

@Controller
public class AssistantController extends BaseController {
	
	@Autowired
	private AssistantService assistantService;
	
	
	/**
	 * 注册注册基本信息接口
	 * @Description:TODO
	 * @return
	 * @author: lifutao
	 * @time:2017年4月28日 下午2:27:53
	 */
	@RequestMapping(value="/assistant/registerAssistant")
	@ResponseBody
	public PageData registerAssistant(){

		PageData pd = new PageData();
		try {
			pd = this.getPageData();
			//非空校验
			boolean hasValue = pd.checkParamHasValue(null);
			if(!hasValue){
				geneParasHasNullPd(pd);
				return pd;
			}
			PageData assistantInfo = assistantService.saveAssistantInfo(pd);
			pd.clear();
			pd.put("errorcode", EnumErrorCode.CODE_000000.code);
			pd.put("errormsg", EnumErrorCode.CODE_000000.msg);
			pd.put("result", "ok");
			pd.put("values", assistantInfo);
		} catch (Exception e) {
			// TODO: handle exception
			pd.clear();
			pd.put("errorcode", EnumErrorCode.CODE_000082.code);
			pd.put("errormsg", EnumErrorCode.CODE_000082.msg);
			pd.put("result", "error");
			pd.put("values", "");
		}
		return pd;
	}
	
	/**
	 * 助手获取自己的信息列表
	 * @Description:TODO
	 * @return
	 * @author: lifutao
	 * @time:2017年4月28日 下午3:38:50
	 */
	@RequestMapping(value="/assistant/getMessageList")
	@ResponseBody
	public PageData getMessageList(){

		PageData pd = new PageData();
		try {
			pd = this.getPageData();
			PageData messageInfo = assistantService.getMessageList(pd);
			pd.clear();
			pd.put("errorcode", EnumErrorCode.CODE_000000.code);
			pd.put("errormsg", EnumErrorCode.CODE_000000.msg);
			pd.put("result", "ok");
			pd.put("values", messageInfo);
		} catch (Exception e) {
			// TODO: handle exception
			pd.clear();
			pd.put("errorcode", EnumErrorCode.CODE_000083.code);
			pd.put("errormsg", EnumErrorCode.CODE_000083.msg);
			pd.put("result", "error");
			pd.put("values", "");
		}
		return pd;
	}
	
	
	/**
	 * 助手获取患者列表
	 * @Description:TODO
	 * @return
	 * @author: lifutao
	 * @time:2017年4月28日 下午4:27:00
	 */
	@RequestMapping(value="/assistant/getPatientList")
	@ResponseBody
	public PageData getPatientList(){

		PageData pd = new PageData();
		try {
			pd = this.getPageData();
			List<PageData> patientList = assistantService.getPatientList(pd);
			pd.clear();
			pd.put("errorcode", EnumErrorCode.CODE_000000.code);
			pd.put("errormsg", EnumErrorCode.CODE_000000.msg);
			pd.put("result", "ok");
			pd.put("values", patientList);
		} catch (Exception e) {
			// TODO: handle exception
			pd.clear();
			pd.put("errorcode", EnumErrorCode.CODE_000084.code);
			pd.put("errormsg", EnumErrorCode.CODE_000084.msg);
			pd.put("result", "error");
			pd.put("values", "");
		}
		return pd;
	}
	
	
	/**
	 * 获取提醒复查页展示的病情诊断，就诊时间及提醒复查时间
	 * @Description:TODO
	 * @return
	 * @author: lifutao
	 * @time:2017年5月2日 下午1:24:22
	 */
	@RequestMapping(value="/assistant/getPatientDetail")
	@ResponseBody
	public PageData getPatientDetail(){

		PageData pd = new PageData();
		try {
			pd = this.getPageData();
			PageData patientDetail = assistantService.getPatientDetail(pd);
			pd.clear();
			pd.put("errorcode", EnumErrorCode.CODE_000000.code);
			pd.put("errormsg", EnumErrorCode.CODE_000000.msg);
			pd.put("result", "ok");
			pd.put("values", patientDetail);
		} catch (Exception e) {
			// TODO: handle exception
			pd.clear();
			pd.put("errorcode", EnumErrorCode.CODE_000085.code);
			pd.put("errormsg", EnumErrorCode.CODE_000085.msg);
			pd.put("result", "error");
			pd.put("values", "");
		}
		return pd;
	}
	
	
	/**
	 * 助手输入手机号，姓名或标签搜索患者
	 * @Description:TODO
	 * @return
	 * @author: lifutao
	 * @time:2017年5月2日 下午1:53:08
	 */
	@RequestMapping(value="/assistant/searchPatient")
	@ResponseBody
	public PageData searchPatient(){

		PageData pd = new PageData();
		try {
			pd = this.getPageData();
			List<PageData> patientList = assistantService.searchPatient(pd);
			pd.clear();
			pd.put("errorcode", EnumErrorCode.CODE_000000.code);
			pd.put("errormsg", EnumErrorCode.CODE_000000.msg);
			pd.put("result", "ok");
			pd.put("values", patientList);
		} catch (Exception e) {
			// TODO: handle exception
			pd.clear();
			pd.put("errorcode", EnumErrorCode.CODE_000086.code);
			pd.put("errormsg", EnumErrorCode.CODE_000086.msg);
			pd.put("result", "error");
			pd.put("values", "");
		}
		return pd;
	}
	
	
	
	
	
	
	
	
	
	
	
}
