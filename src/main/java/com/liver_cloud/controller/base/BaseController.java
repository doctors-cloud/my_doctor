package com.liver_cloud.controller.base;

import javax.servlet.http.HttpServletRequest;

import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.servlet.ModelAndView;

import com.liver_cloud.entity.Page;
import com.liver_cloud.util.EnumErrorCode;
import com.liver_cloud.util.Logger;
import com.liver_cloud.util.PageData;
import com.liver_cloud.util.UuidUtil;

public class BaseController {

	protected Logger logger = Logger.getLogger(this.getClass());

	/**
	 * 得到PageData
	 */
	public PageData getPageData() {
		return new PageData(this.getRequest());
	}

	/**
	 * 得到ModelAndView
	 */
	public ModelAndView getModelAndView() {
		return new ModelAndView();
	}

	/**
	 * 得到request对象
	 */
	public HttpServletRequest getRequest() {
		HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder
				.getRequestAttributes()).getRequest();

		return request;
	}

	/**
	 * 得到32位的uuid
	 * 
	 * @return
	 */
	public String get32UUID() {

		return UuidUtil.get32UUID();
	}

	/**
	 * 得到分页列表的信息
	 */
	public Page getPage() {

		return new Page();
	}

	public static void logBefore(Logger logger, String interfaceName) {
		logger.info("");
		logger.info("start");
		logger.info(interfaceName);
	}

	public static void logAfter(Logger logger) {
		logger.info("end");
		logger.info("");
	}
	
	public void geneParasHasNullPd(PageData pd){
		String errorMsg="参数有空值,"+pd.toString();
		System.out.println(errorMsg);
		pd.clear();
		pd.put("errorcode", EnumErrorCode.CODE_000301.code);
		pd.put("errormsg", errorMsg);
		pd.put("result", "error");
		pd.put("values", "");
	}
	

}
