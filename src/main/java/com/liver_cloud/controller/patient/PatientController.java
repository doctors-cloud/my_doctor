package com.liver_cloud.controller.patient;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileUploadException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSONObject;
import com.liver_cloud.controller.base.BaseController;
import com.liver_cloud.service.doctor.DoctorService;
import com.liver_cloud.service.patient.PatientService;
import com.liver_cloud.util.EnumErrorCode;
import com.liver_cloud.util.PageData;
import com.liver_cloud.util.StringUtil;

@Controller
public class PatientController extends BaseController {

	@Autowired
	private DoctorService doctorService;

	@Autowired
	private PatientService patientService;

	/**
	 * 患者获取信息列表接口
	 * 
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/patient/getPatientMessage")
	@ResponseBody
	public PageData getPatientMessage() throws Exception {

		PageData pd = new PageData();
		try {
			pd = this.getPageData();
			PageData result = patientService.getPatientMessage(pd);
			pd.clear();
			pd.put("errorcode", EnumErrorCode.CODE_000000.code);
			pd.put("errormsg", EnumErrorCode.CODE_000000.msg);
			pd.put("result", "ok");
			pd.put("values", result);
		} catch (Exception e) {
			// TODO: handle exception
			pd.clear();
			pd.put("errorcode", EnumErrorCode.CODE_000008.code);
			pd.put("errormsg", EnumErrorCode.CODE_000008.msg);
			pd.put("result", "error");
			pd.put("values", "");
		}
		return pd;
	}

	/**
	 * 患者获取我的问题接口
	 * 
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/patient/getQuestionDetail")
	@ResponseBody
	public PageData getQuestionDetail() throws Exception {

		PageData pd = new PageData();
		try {
			pd = this.getPageData();
			PageData result = patientService.getQuestionDetail(pd);
			pd.clear();
			pd.put("errorcode", EnumErrorCode.CODE_000000.code);
			pd.put("errormsg", EnumErrorCode.CODE_000000.msg);
			pd.put("result", "ok");
			pd.put("values", result);
		} catch (Exception e) {
			// TODO: handle exception
			pd.clear();
			pd.put("errorcode", EnumErrorCode.CODE_000014.code);
			pd.put("errormsg", EnumErrorCode.CODE_000014.msg);
			pd.put("result", "error");
			pd.put("values", "");
		}
		return pd;
	}

	/**
	 * 患者回复医生追问接口
	 * 
	 * @return
	 */
	@RequestMapping(value = "/patient/replyQuestion")
	@ResponseBody
	public PageData replyQuestion() {

		PageData pd = new PageData();
		try {
			pd = this.getPageData();
			// 保存数据走事务
			PageData replyInfo = patientService.saveReplyContent(pd);
			pd.clear();
			if (replyInfo == null) {
				pd.put("errorcode", EnumErrorCode.CODE_000000.code);
				pd.put("errormsg", EnumErrorCode.CODE_000000.msg);
				pd.put("result", "ok");
				pd.put("values", "ok");
			} else {
				pd.put("errorcode", EnumErrorCode.CODE_000062.code);
				pd.put("errormsg", EnumErrorCode.CODE_000062.msg);
				pd.put("result", "error");
				pd.put("values", "error");
			}
		} catch (Exception e) {
			// TODO: handle exception
			pd.clear();
			pd.put("errorcode", EnumErrorCode.CODE_000010.code);
			pd.put("errormsg", EnumErrorCode.CODE_000010.msg);
			pd.put("result", "error");
			pd.put("values", "");
		}
		return pd;
	}

	/**
	 * 患者查看解决方案详情接口
	 * 
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/patient/getDoctorSolution")
	@ResponseBody
	public PageData getDoctorSolution() throws Exception {

		PageData pd = new PageData();
		try {
			pd = this.getPageData();
			PageData solutionDetail = patientService.getDoctorSolution(pd);
			pd.clear();
			pd.put("errorcode", EnumErrorCode.CODE_000000.code);
			pd.put("errormsg", EnumErrorCode.CODE_000000.msg);
			pd.put("result", "ok");
			pd.put("values", solutionDetail);
		} catch (Exception e) {
			// TODO: handle exception
			pd.clear();
			pd.put("errorcode", EnumErrorCode.CODE_000009.code);
			pd.put("errormsg", EnumErrorCode.CODE_000009.msg);
			pd.put("result", "error");
			pd.put("values", "");
		}
		return pd;
	}

	/**
	 * 患者找医生获取病症列表接口
	 * 
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/patient/getRecommendModle")
	@ResponseBody
	public PageData getRecommendModle() throws Exception {

		PageData pd = new PageData();
		try {
			pd = this.getPageData();
			List<PageData> modleList = patientService.getRecommendModle(pd);
			pd.clear();
			pd.put("errorcode", EnumErrorCode.CODE_000000.code);
			pd.put("errormsg", EnumErrorCode.CODE_000000.msg);
			pd.put("result", "ok");
			pd.put("values", modleList);
		} catch (Exception e) {
			// TODO: handle exception
			pd.clear();
			pd.put("errorcode", EnumErrorCode.CODE_000031.code);
			pd.put("errormsg", EnumErrorCode.CODE_000031.msg);
			pd.put("result", "error");
			pd.put("values", "");
		}
		return pd;
	}

	/**
	 * 患者获取推荐医生列表接口
	 * 
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/patient/getRecommendDoctorList")
	@ResponseBody
	public PageData getRecommendDoctorList() throws Exception {

		PageData pd = new PageData();
		try {
			pd = this.getPageData();
			System.out.println(pd);
			List<PageData> doctorList = patientService
					.getRecommendDoctorList(pd);
			pd.clear();
			pd.put("errorcode", EnumErrorCode.CODE_000000.code);
			pd.put("errormsg", EnumErrorCode.CODE_000000.msg);
			pd.put("result", "ok");
			pd.put("values", doctorList);
		} catch (Exception e) {
			// TODO: handle exception
			pd.clear();
			pd.put("errorcode", EnumErrorCode.CODE_000032.code);
			pd.put("errormsg", EnumErrorCode.CODE_000032.msg);
			pd.put("result", "error");
			pd.put("values", "");
		}
		return pd;
	}

	/**
	 * 患者精确查找医生接口
	 * 
	 * @return
	 */
	@RequestMapping(value = "/patient/getDoctorList")
	@ResponseBody
	public PageData getDoctorList() {

		PageData pd = new PageData();
		try {
			pd = this.getPageData();
			List<PageData> doctorList = patientService.getDoctorList(pd);
			pd.clear();
			pd.put("errorcode", EnumErrorCode.CODE_000000.code);
			pd.put("errormsg", EnumErrorCode.CODE_000000.msg);
			pd.put("result", "ok");
			pd.put("values", doctorList);
		} catch (Exception e) {
			// TODO: handle exception
			pd.clear();
			pd.put("errorcode", EnumErrorCode.CODE_000013.code);
			pd.put("errormsg", EnumErrorCode.CODE_000013.msg);
			pd.put("result", "error");
			pd.put("values", "");
		}
		return pd;
	}

	/**
	 * 患者点击咨询按钮获取咨询模板
	 * 
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/patient/getQuestionTemplateList")
	@ResponseBody
	public PageData getQuestionTemplateList() throws Exception {

		PageData pd = new PageData();
		try {
			pd = this.getPageData();
			PageData questionTemplateList = patientService
					.getQuestionTemplateList(pd);
			pd.clear();
			pd.put("errorcode", EnumErrorCode.CODE_000000.code);
			pd.put("errormsg", EnumErrorCode.CODE_000000.msg);
			pd.put("result", "ok");
			pd.put("values", questionTemplateList);
		} catch (Exception e) {
			// TODO: handle exception
			pd.clear();
			pd.put("errorcode", EnumErrorCode.CODE_000015.code);
			pd.put("errormsg", EnumErrorCode.CODE_000015.msg);
			pd.put("result", "error");
			pd.put("values", "");
		}
		return pd;
	}

	/**
	 * 患者提交问题接口
	 * 
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/patient/upLoadQuestion", method = RequestMethod.POST)
	@ResponseBody
	public PageData upLoadQuestion(HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		PageData pd = new PageData();
		try {
			JSONObject param = StringUtil.getJSONObject(request);
			patientService.savePatientQuestion(param);
			pd.clear();
			pd.put("errorcode", EnumErrorCode.CODE_000000.code);
			pd.put("errormsg", EnumErrorCode.CODE_000000.msg);
			pd.put("result", "ok");
			pd.put("values", "ok");
		} catch (Exception e) {
			// TODO: handle exception
			pd.clear();
			pd.put("errorcode", EnumErrorCode.CODE_000016.code);
			pd.put("errormsg", EnumErrorCode.CODE_000016.msg);
			pd.put("result", "error");
			pd.put("values", "");
		}
		return pd;
	}

	/**
	 * 精确查询医生时获取职称列表
	 */
	@RequestMapping(value = "/common/getProfessionNameList")
	@ResponseBody
	public PageData getProfessionNameList() {

		PageData pd = new PageData();
		try {
			pd = this.getPageData();
			List<PageData> professionList = patientService
					.getProfessionNameList(pd);
			pd.clear();
			pd.put("errorcode", EnumErrorCode.CODE_000000.code);
			pd.put("errormsg", EnumErrorCode.CODE_000000.msg);
			pd.put("result", "ok");
			pd.put("values", professionList);
		} catch (Exception e) {
			// TODO: handle exception
			pd.clear();
			pd.put("errorcode", EnumErrorCode.CODE_000012.code);
			pd.put("errormsg", EnumErrorCode.CODE_000012.msg);
			pd.put("result", "error");
			pd.put("values", "");
		}
		return pd;
	}

	/**
	 * 获取科室列表接口
	 * 
	 * @return
	 */
	@RequestMapping(value = "/common/getDepartMentList")
	@ResponseBody
	public PageData getDepartMentList() {

		PageData pd = new PageData();
		try {
			pd = this.getPageData();
			List<PageData> departmentList = patientService
					.getDepartMentList(pd);
			pd.clear();
			pd.put("errorcode", EnumErrorCode.CODE_000000.code);
			pd.put("errormsg", EnumErrorCode.CODE_000000.msg);
			pd.put("result", "ok");
			pd.put("values", departmentList);
		} catch (Exception e) {
			// TODO: handle exception
			pd.clear();
			pd.put("errorcode", EnumErrorCode.CODE_000011.code);
			pd.put("errormsg", EnumErrorCode.CODE_000011.msg);
			pd.put("result", "error");
			pd.put("values", "");
		}
		return pd;
	}

	/**
	 * 患者病例中获取诊断列表接口
	 * 
	 * @return
	 */
	@RequestMapping(value = "/common/getDiagnosisList")
	@ResponseBody
	public PageData getDiagnosisList() {

		PageData pd = new PageData();
		try {
			pd = this.getPageData();
			List<PageData> diagnosisList = patientService.getDiagnosisList(pd);
			pd.clear();
			pd.put("errorcode", EnumErrorCode.CODE_000000.code);
			pd.put("errormsg", EnumErrorCode.CODE_000000.msg);
			pd.put("result", "ok");
			pd.put("values", diagnosisList);
		} catch (Exception e) {
			// TODO: handle exception
			pd.clear();
			pd.put("errorcode", EnumErrorCode.CODE_000023.code);
			pd.put("errormsg", EnumErrorCode.CODE_000023.msg);
			pd.put("result", "error");
			pd.put("values", "");
		}
		return pd;
	}

	/**
	 * 患者添加诊断接口
	 * 
	 * @return
	 */
	@RequestMapping(value = "/patient/addDiagnosisType")
	@ResponseBody
	public PageData addDiagnosisType() {

		PageData pd = new PageData();
		try {
			pd = this.getPageData();
			patientService.saveDiagnosisType(pd);
			pd.clear();
			pd.put("errorcode", EnumErrorCode.CODE_000000.code);
			pd.put("errormsg", EnumErrorCode.CODE_000000.msg);
			pd.put("result", "ok");
			pd.put("values", "ok");
		} catch (Exception e) {
			// TODO: handle exception
			pd.clear();
			pd.put("errorcode", EnumErrorCode.CODE_000025.code);
			pd.put("errormsg", EnumErrorCode.CODE_000025.msg);
			pd.put("result", "error");
			pd.put("values", "");
		}
		return pd;
	}

	/**
	 * 患者获取病例首页接口
	 * 
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/patient/getPatientCasePage")
	@ResponseBody
	public PageData getPatientCasePage() throws Exception {

		PageData pd = new PageData();
		try {
			pd = this.getPageData();
			PageData casePage = patientService.getPatientCasePage(pd);
			pd.clear();
			pd.put("errorcode", EnumErrorCode.CODE_000000.code);
			pd.put("errormsg", EnumErrorCode.CODE_000000.msg);
			pd.put("result", "ok");
			pd.put("values", casePage);
		} catch (Exception e) {
			// TODO: handle exception
			pd.clear();
			pd.put("errorcode", EnumErrorCode.CODE_000024.code);
			pd.put("errormsg", EnumErrorCode.CODE_000024.msg);
			pd.put("result", "error");
			pd.put("values", "");
		}
		return pd;
	}

	/**
	 * 获取患者一周热量
	 * 
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/patient/getCaloryOfWeek")
	@ResponseBody
	public PageData getCaloryOfWeek() throws Exception {

		PageData pd = new PageData();
		try {
			pd = this.getPageData();
			List<PageData> caloryOfWeek = patientService.getCaloryOfWeek(pd);
			pd.clear();
			pd.put("errorcode", EnumErrorCode.CODE_000000.code);
			pd.put("errormsg", EnumErrorCode.CODE_000000.msg);
			pd.put("result", "ok");
			pd.put("values", caloryOfWeek);
		} catch (Exception e) {
			// TODO: handle exception
			pd.clear();
			pd.put("errorcode", EnumErrorCode.CODE_000026.code);
			pd.put("errormsg", EnumErrorCode.CODE_000026.msg);
			pd.put("result", "error");
			pd.put("values", "");
		}
		return pd;
	}

	/**
	 * 患者上传化验单接口
	 * 
	 * @return
	 * @throws FileUploadException
	 * @throws IOException
	 * @throws ServletException
	 */
	@RequestMapping(value = "/patient/addTestReprot")
	@ResponseBody
	public PageData addTestReport(HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		PageData pd = new PageData();
		try {
			pd = this.getPageData();
			PageData reportUrl = patientService.savePatientTestReport(request);			
			pd.clear();
			pd.put("errorcode", EnumErrorCode.CODE_000000.code);
			pd.put("errormsg", EnumErrorCode.CODE_000000.msg);
			pd.put("result", "ok");
			pd.put("values", reportUrl);
		} catch (Exception e) {
			// TODO: handle exception
			pd.clear();
			pd.put("errorcode", EnumErrorCode.CODE_000030.code);
			pd.put("errormsg", EnumErrorCode.CODE_000030.msg);
			pd.put("result", "error");
			pd.put("values", "");
		}
		return pd;
	}

	@RequestMapping(value = "/patient/getAddressPartmentAndProfessionName")
	@ResponseBody
	public PageData getAddressPartmentAndProfessionName() {

		PageData pd = new PageData();
		try {
			pd = this.getPageData();
			PageData info = patientService
					.getAddressPartmentAndProfessionName();
			pd.clear();
			pd.put("errorcode", EnumErrorCode.CODE_000000.code);
			pd.put("errormsg", EnumErrorCode.CODE_000000.msg);
			pd.put("result", "ok");
			pd.put("values", info);
		} catch (Exception e) {
			// TODO: handle exception
			pd.clear();
			pd.put("errorcode", EnumErrorCode.CODE_000034.code);
			pd.put("errormsg", EnumErrorCode.CODE_000034.msg);
			pd.put("result", "error");
			pd.put("values", "");
		}
		return pd;
	}

	/**
	 * 患者删除病症接口
	 * 
	 * @return
	 */
	@RequestMapping(value = "/patient/deleteDiagnosis")
	@ResponseBody
	public PageData deleteDiagnosis() {

		PageData pd = new PageData();
		try {
			pd = this.getPageData();
			patientService.deleteDiagnosis(pd);
			pd.clear();
			pd.put("errorcode", EnumErrorCode.CODE_000000.code);
			pd.put("errormsg", EnumErrorCode.CODE_000000.msg);
			pd.put("result", "ok");
			pd.put("values", "ok");
		} catch (Exception e) {
			// TODO: handle exception
			pd.clear();
			pd.put("errorcode", EnumErrorCode.CODE_000040.code);
			pd.put("errormsg", EnumErrorCode.CODE_000040.msg);
			pd.put("result", "error");
			pd.put("values", "");
		}
		return pd;
	}

	/**
	 * 患者修改化验单时间接口
	 * 
	 * @return
	 */
	@RequestMapping(value = "/patient/modifyReprotTime")
	@ResponseBody
	public PageData modifyReprotTime() {

		PageData pd = new PageData();
		try {
			pd = this.getPageData();
			patientService.modifyReprotTime(pd);
			pd.clear();
			pd.put("errorcode", EnumErrorCode.CODE_000000.code);
			pd.put("errormsg", EnumErrorCode.CODE_000000.msg);
			pd.put("result", "ok");
			pd.put("values", "ok");
		} catch (Exception e) {
			// TODO: handle exception
			pd.clear();
			pd.put("errorcode", EnumErrorCode.CODE_000042.code);
			pd.put("errormsg", EnumErrorCode.CODE_000042.msg);
			pd.put("result", "error");
			pd.put("values", "");
		}
		return pd;
	}

	/**
	 * 患者删除病例接口
	 * 
	 * @return
	 */
	@RequestMapping(value = "/patient/deleteTestReport")
	@ResponseBody
	public PageData deleteTestReport() {

		PageData pd = new PageData();
		try {
			pd = this.getPageData();
			patientService.deleteTestReport(pd);
			pd.clear();
			pd.put("errorcode", EnumErrorCode.CODE_000000.code);
			pd.put("errormsg", EnumErrorCode.CODE_000000.msg);
			pd.put("result", "ok");
			pd.put("values", "ok");
		} catch (Exception e) {
			// TODO: handle exception
			pd.clear();
			pd.put("errorcode", EnumErrorCode.CODE_000043.code);
			pd.put("errormsg", EnumErrorCode.CODE_000043.msg);
			pd.put("result", "error");
			pd.put("values", "");
		}
		return pd;
	}

	/**
	 * 患者信息状态是否更改接口
	 * 
	 * @return
	 */
	@RequestMapping(value = "/patient/getPatientMessageChanged")
	@ResponseBody
	public PageData getPatientMessageChagend() {

		PageData pd = new PageData();
		try {
			pd = this.getPageData();
			PageData messageChanged = patientService
					.getPatientMessageChanged(pd);
			pd.clear();
			pd.put("errorcode", EnumErrorCode.CODE_000000.code);
			pd.put("errormsg", EnumErrorCode.CODE_000000.msg);
			pd.put("result", "ok");
			pd.put("values", messageChanged);
		} catch (Exception e) {
			// TODO: handle exception
			pd.clear();
			pd.put("errorcode", EnumErrorCode.CODE_000057.code);
			pd.put("errormsg", EnumErrorCode.CODE_000057.msg);
			pd.put("result", "error");
			pd.put("values", "");
		}
		return pd;
	}

	/**
	 * 在病症列表里添加诊断
	 * 
	 * @return
	 */
	@RequestMapping(value = "/patient/addUserDefinedDiagnosisType")
	@ResponseBody
	public PageData addUserDefinedDiagnosisType() {

		PageData pd = new PageData();
		try {
			pd = this.getPageData();
			patientService.saveUserDefinedDiagnosisType(pd);
			pd.clear();
			pd.put("errorcode", EnumErrorCode.CODE_000000.code);
			pd.put("errormsg", EnumErrorCode.CODE_000000.msg);
			pd.put("result", "ok");
			pd.put("values", "ok");
		} catch (Exception e) {
			// TODO: handle exception
			pd.clear();
			pd.put("errorcode", EnumErrorCode.CODE_000059.code);
			pd.put("errormsg", EnumErrorCode.CODE_000059.msg);
			pd.put("result", "error");
			pd.put("values", "");
		}
		return pd;
	}

	@RequestMapping(value = "/patient/deleteUserDefinedDiagnosis")
	@ResponseBody
	public PageData deleteUserDefinedDiagnosis() {

		PageData pd = new PageData();
		try {
			pd = this.getPageData();
			patientService.deleteUserDefinedDiagnosis(pd);
			pd.clear();
			pd.put("errorcode", EnumErrorCode.CODE_000000.code);
			pd.put("errormsg", EnumErrorCode.CODE_000000.msg);
			pd.put("result", "ok");
			pd.put("values", "ok");
		} catch (Exception e) {
			// TODO: handle exception
			pd.clear();
			pd.put("errorcode", EnumErrorCode.CODE_000060.code);
			pd.put("errormsg", EnumErrorCode.CODE_000060.msg);
			pd.put("result", "error");
			pd.put("values", "");
		}
		return pd;
	}
	
	
	/**
	 * 获取当前日期起前七天的数据
	 * @return
	 */
	@RequestMapping(value = "/patient/getPeriodTime")
	@ResponseBody
	public PageData getPeriodTime() {

		PageData pd = new PageData();
		try {
			pd = this.getPageData();
			PageData periodTime =  patientService.getPeriodTime();
			pd.clear();
			pd.put("errorcode", EnumErrorCode.CODE_000000.code);
			pd.put("errormsg", EnumErrorCode.CODE_000000.msg);
			pd.put("result", "ok");
			pd.put("values", periodTime);
		} catch (Exception e) {
			// TODO: handle exception
			pd.clear();
			pd.put("errorcode", EnumErrorCode.CODE_000063.code);
			pd.put("errormsg", EnumErrorCode.CODE_000063.msg);
			pd.put("result", "error");
			pd.put("values", "");
		}
		return pd;
	}
	
	
	/**
	 * 上传一周热量
	 * @return
	 */
	@RequestMapping(value = "/patient/saveCalory")
	@ResponseBody
	public PageData saveCalory(HttpServletRequest request,
			HttpServletResponse response){

		PageData pd = new PageData();
		try {
			JSONObject param = StringUtil.getJSONObject(request);
			patientService.saveCalory(param);
			pd.clear();
			pd.put("errorcode", EnumErrorCode.CODE_000000.code);
			pd.put("errormsg", EnumErrorCode.CODE_000000.msg);
			pd.put("result", "ok");
			pd.put("values", "ok");
		} catch (Exception e) {
			// TODO: handle exception
			pd.clear();
			pd.put("errorcode", EnumErrorCode.CODE_000064.code);
			pd.put("errormsg", EnumErrorCode.CODE_000064.msg);
			pd.put("result", "error");
			pd.put("values", "");
		}
		return pd;
	}
		
	
	/**
	 * 获取压缩前的图片url
	 * @return
	 */
	@RequestMapping(value = "/patient/getRealReportUrl")
	@ResponseBody
	public PageData getRealReportPicture(){

		PageData pd = new PageData();
		try {
			pd = this.getPageData();
			PageData realPictureUrl = patientService.getRealReportPicture(pd);
			pd.clear();
			pd.put("errorcode", EnumErrorCode.CODE_000000.code);
			pd.put("errormsg", EnumErrorCode.CODE_000000.msg);
			pd.put("result", "ok");
			pd.put("values", realPictureUrl);
		} catch (Exception e) {
			// TODO: handle exception
			pd.clear();
			pd.put("errorcode", EnumErrorCode.CODE_000065.code);
			pd.put("errormsg", EnumErrorCode.CODE_000065.msg);
			pd.put("result", "error");
			pd.put("values", "");
		}
		return pd;
	}
	
	/**
	 * 
	 * @Description:获取医生详情
	 * @return
	 * @author: lifutao
	 * @time:2017年3月16日 上午10:07:33
	 */
	@RequestMapping(value="/patient/getDoctorInfo")
	@ResponseBody
	public PageData getDoctorInfo(){
		
		PageData pd = new PageData();		
	try {
			pd = this.getPageData();
			PageData doctorInfo = doctorService.getDoctorInfo(pd);
			pd.clear();
			pd.put("errorcode",  EnumErrorCode.CODE_000000.code);
			pd.put("errormsg", EnumErrorCode.CODE_000000.msg);
			pd.put("result", "ok");
			pd.put("values", doctorInfo);
		} catch (Exception e) {
			// TODO: handle exception
			pd.clear();
			pd.put("errorcode",  EnumErrorCode.CODE_000066.code);
			pd.put("errormsg", EnumErrorCode.CODE_000066.msg);
			pd.put("result", "error");
			pd.put("values", "");
		}		
		return pd;	
	}
	
	
	/**
	 * 患者接受复查通知
	 * @Description:TODO
	 * @return
	 * @author: lifutao
	 * @time:2017年5月2日 下午2:47:27
	 */
	@RequestMapping(value="/patient/getAlertInfo")
	@ResponseBody
	public PageData getAlertInfo(){
		
		PageData pd = new PageData();		
	try {
			pd = this.getPageData();
			PageData  alertInfo = patientService.getAlertInfo(pd);
			pd.clear();
			pd.put("errorcode",  EnumErrorCode.CODE_000000.code);
			pd.put("errormsg", EnumErrorCode.CODE_000000.msg);
			pd.put("result", "ok");
			pd.put("values", alertInfo);
		} catch (Exception e) {
			// TODO: handle exception
			pd.clear();
			pd.put("errorcode",  EnumErrorCode.CODE_000087.code);
			pd.put("errormsg", EnumErrorCode.CODE_000087.msg);
			pd.put("result", "error");
			pd.put("values", "");
		}		
		return pd;	
	}
}
