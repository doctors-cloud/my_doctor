package com.liver_cloud.controller.common;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.liver_cloud.controller.base.BaseController;
import com.liver_cloud.service.common.CommonService;
import com.liver_cloud.service.doctor.DoctorService;
import com.liver_cloud.service.patient.PatientService;
import com.liver_cloud.util.EnumErrorCode;
import com.liver_cloud.util.PageData;

@Controller
public class CommonController extends BaseController{
	
	@Autowired
	private DoctorService doctorService;

	@Autowired
	private PatientService patientService;
	
	@Autowired
	private CommonService commonService;
	
	/**
	 * 
	 * @Description:助手，患者，医生点击推送获取推送信息列表
	 * @return
	 * @author: lifutao
	 * @time:2017年4月18日 下午2:35:56
	 */
	@RequestMapping(value = "/common/getPushMessageList")
	@ResponseBody
	public PageData getPushMessageList(){
		PageData pd = new PageData();
		try {
			pd = this.getPageData();
			List<PageData> pushMessageList = commonService.getPushMessageList(pd);
			pd.clear();
			pd.put("errorcode", EnumErrorCode.CODE_000000.code);
			pd.put("errormsg", EnumErrorCode.CODE_000000.msg);
			pd.put("result", "ok");
			pd.put("values", pushMessageList);			
		} catch (Exception e) {
			// TODO: handle exception
			pd.clear();
			pd.put("errorcode", EnumErrorCode.CODE_000067.code);
			pd.put("errormsg", EnumErrorCode.CODE_000067.msg);
			pd.put("result", "error");
			pd.put("values", "");
		}
		
		return pd;
		
	}
	
	/**
	 * 获取推送信息详情
	 * @Description:TODO
	 * @return
	 * @author: lifutao
	 * @time:2017年4月19日 下午2:54:30
	 */
	@RequestMapping(value = "/common/getPushMessageDetail")
	@ResponseBody
	public PageData getPushMessageDetail(){
		PageData pd = new PageData();
		try {
			pd = this.getPageData();
			PageData messageDetail = commonService.updateAndGetPushMessageDetail(pd);
			pd.clear();
			pd.put("errorcode", EnumErrorCode.CODE_000000.code);
			pd.put("errormsg", EnumErrorCode.CODE_000000.msg);
			pd.put("result", "ok");
			pd.put("values", messageDetail);			
		} catch (Exception e) {
			// TODO: handle exception
			pd.clear();
			pd.put("errorcode", EnumErrorCode.CODE_000068.code);
			pd.put("errormsg", EnumErrorCode.CODE_000068.msg);
			pd.put("result", "error");
			pd.put("values", "");
		}
		
		return pd;
		
	}
	
	/**
	 * 为推送消息添加评论
	 * @Description:TODO
	 * @return
	 * @author: lifutao
	 * @time:2017年4月20日 上午11:54:40
	 */
	@RequestMapping(value = "/common/leaveMessage")
	@ResponseBody
	public PageData leaveMessage(){
		PageData pd = new PageData();
		try {
			pd = this.getPageData();
			commonService.insertLeaveMessage(pd);
			pd.clear();
			pd.put("errorcode", EnumErrorCode.CODE_000000.code);
			pd.put("errormsg", EnumErrorCode.CODE_000000.msg);
			pd.put("result", "ok");
			pd.put("values", "ok");			
		} catch (Exception e) {
			// TODO: handle exception
			pd.clear();
			pd.put("errorcode", EnumErrorCode.CODE_000069.code);
			pd.put("errormsg", EnumErrorCode.CODE_000069.msg);
			pd.put("result", "error");
			pd.put("values", "");
		}		
		return pd;		
	}
	
	/**
	 * 为推送消息点赞
	 * @Description:TODO
	 * @return
	 * @author: lifutao
	 * @time:2017年4月20日 下午1:23:00
	 */
	@RequestMapping(value = "/common/supportPushMessage")
	@ResponseBody
	public PageData supportPushMessage(){
		PageData pd = new PageData();
		try {
			pd = this.getPageData();
			commonService.insertSupportPushMessage(pd);
			pd.clear();
			pd.put("errorcode", EnumErrorCode.CODE_000000.code);
			pd.put("errormsg", EnumErrorCode.CODE_000000.msg);
			pd.put("result", "ok");
			pd.put("values", "ok");			
		} catch (Exception e) {
			// TODO: handle exception
			pd.clear();
			pd.put("errorcode", EnumErrorCode.CODE_000070.code);
			pd.put("errormsg", EnumErrorCode.CODE_000070.msg);
			pd.put("result", "error");
			pd.put("values", "");
		}		
		return pd;		
	}
	
	/**
	 * 为留言点赞
	 * @Description:TODO
	 * @return
	 * @author: lifutao
	 * @time:2017年4月20日 下午1:49:11
	 */
	@RequestMapping(value = "/common/supportLeaveMessage")
	@ResponseBody
	public PageData supportLeaveMessage(){
		PageData pd = new PageData();
		try {
			pd = this.getPageData();
			commonService.insertSupportLeaveMessage(pd);
			pd.clear();
			pd.put("errorcode", EnumErrorCode.CODE_000000.code);
			pd.put("errormsg", EnumErrorCode.CODE_000000.msg);
			pd.put("result", "ok");
			pd.put("values", "ok");			
		} catch (Exception e) {
			// TODO: handle exception
			pd.clear();
			pd.put("errorcode", EnumErrorCode.CODE_000071.code);
			pd.put("errormsg", EnumErrorCode.CODE_000071.msg);
			pd.put("result", "error");
			pd.put("values", "");
		}		
		return pd;		
	}
	
	/**
	 * 回复留言
	 * @Description:TODO
	 * @return
	 * @author: lifutao
	 * @time:2017年4月20日 下午1:59:45
	 */
	@RequestMapping(value = "/common/replyLeaveMessage")
	@ResponseBody
	public PageData replyLeaveMessage(){
		PageData pd = new PageData();
		try {
			pd = this.getPageData();
			commonService.insertReplyLeaveMessage(pd);
			pd.clear();
			pd.put("errorcode", EnumErrorCode.CODE_000000.code);
			pd.put("errormsg", EnumErrorCode.CODE_000000.msg);
			pd.put("result", "ok");
			pd.put("values", "ok");			
		} catch (Exception e) {
			// TODO: handle exception
			pd.clear();
			pd.put("errorcode", EnumErrorCode.CODE_000072.code);
			pd.put("errormsg", EnumErrorCode.CODE_000072.msg);
			pd.put("result", "error");
			pd.put("values", "");
		}		
		return pd;		
	}
	
	/**
	 * 医生或助手发布推送消息
	 * @Description:TODO
	 * @return
	 * @author: lifutao
	 * @time:2017年4月20日 下午5:53:57
	 */
	@RequestMapping(value = "/common/pushMessage")
	@ResponseBody
	public PageData pushMessage(HttpServletRequest request,
			HttpServletResponse response){
		PageData pd = new PageData();
		try {
			PageData result = commonService.saveOrUpdatePushMessage(request);
			pd.put("errorcode", EnumErrorCode.CODE_000000.code);
			pd.put("errormsg", EnumErrorCode.CODE_000000.msg);
			pd.put("result", "ok");
			pd.put("values", result);			
		} catch (Exception e) {
			// TODO: handle exception
			pd.clear();
			pd.put("errorcode", EnumErrorCode.CODE_000073.code);
			pd.put("errormsg", EnumErrorCode.CODE_000073.msg);
			pd.put("result", "error");
			pd.put("values", "");
		}		
		return pd;		
	}
	
	
	
	
	
	
	
	
	
	
}
