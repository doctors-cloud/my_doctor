package com.liver_cloud.controller.doctor;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.liver_cloud.controller.base.BaseController;
import com.liver_cloud.model.ServiceResult;
import com.liver_cloud.service.doctor.DoctorService2;
import com.liver_cloud.service.patient.PatientService;
import com.liver_cloud.util.EnumErrorCode;
import com.liver_cloud.util.PageData;

@Controller
public class DoctorController2 extends BaseController{

	
	@Autowired
	private DoctorService2 doctorService;

	@Autowired
	private PatientService patientService;
	
	
	/**
	 * 2医生删除患者
	 * 
	 * @return
	 */
	@RequestMapping(value = "/doctor/deletePatient")
	@ResponseBody
	public PageData deletePatient() {
		PageData pd = new PageData();
		try {
			pd = this.getPageData();
			String patientIds = (String) pd.get("patientId");
			String doctorId = (String) pd.get("doctorId");
			//非空校验
			boolean hasValue = pd.checkParamHasValue(null);
			if(!hasValue){
				geneParasHasNullPd(pd);
				return pd;
			}
			String[] patientIdArray = patientIds.split(",");
			
			doctorService.deletePatient(doctorId,patientIdArray);
			pd.clear();
			pd.put("errorcode", EnumErrorCode.CODE_000000.code);
			pd.put("errormsg", EnumErrorCode.CODE_000000.msg);
			pd.put("result", "ok");
			pd.put("values", "ok");
		} catch (Exception e) {
			// TODO: handle exception
			pd.clear();
			pd.put("errorcode", EnumErrorCode.CODE_000001.code);
			pd.put("errormsg", e.getMessage());
			pd.put("result", "error");
			pd.put("values", "");
		}
		return pd;
		
		
	}
	
	/**
	 * 3医生删除自定义标签接口
	 * 
	 * @return
	 */
	@RequestMapping(value = "/doctor/deletePatientTitel")
	@ResponseBody
	public PageData deletePatientTitel() {
		PageData pd = new PageData();
		try {
			pd = this.getPageData();
			//非空校验
			boolean hasValue = pd.checkParamHasValue(null);
			if(!hasValue){
				geneParasHasNullPd(pd);
				return pd;
			}
			// 调用服务层代码
			doctorService.deletePatientTitel(pd);
			pd.clear();
			pd.put("errorcode", EnumErrorCode.CODE_000000.code);
			pd.put("errormsg", EnumErrorCode.CODE_000000.msg);
			pd.put("result", "ok");
			// 
			pd.put("values", "ok");
		} catch (Exception e) {
			e.printStackTrace();
			pd.clear();
			pd.put("errorcode", EnumErrorCode.CODE_000001.code);
			pd.put("errormsg", e.getMessage());
			pd.put("result", "error");
			pd.put("values", "");
		}
		return pd;
		
		
	}
	

	/**
	 * 4医生创建标签接口
	 * 
	 * @return
	 */
	@RequestMapping(value = "/doctor/addPatientTitel")
	@ResponseBody
	public PageData addPatientTitel() {
		PageData pd = new PageData();
		try {
			pd = this.getPageData();
			//非空校验
			boolean hasValue = pd.checkParamHasValue(null);
			if(!hasValue){
				geneParasHasNullPd(pd);
				return pd;
			}
			// TODO
			PageData addPatientTitel = doctorService.addPatientTitel(pd);
			if(addPatientTitel!=null){
				return addPatientTitel;
			}
			
			pd.clear();
			pd.put("errorcode", EnumErrorCode.CODE_000000.code);
			pd.put("errormsg", EnumErrorCode.CODE_000000.msg);
			pd.put("result", "ok");
			// TODO
			pd.put("values", "");
		} catch (Exception e) {
			e.printStackTrace();
			pd.clear();
			pd.put("errorcode", EnumErrorCode.CODE_000001.code);
			pd.put("errormsg", e.getMessage());
			pd.put("result", "error");
			pd.put("values", "");
		}
		return pd;
		
		
	}
	
	/**
	 * 5获取患者总人数接口
	 * 
	 * @return
	 */
	@RequestMapping(value = "/doctor/getPatientNum")
	@ResponseBody
	public PageData getPatientNum() {
		PageData pd = new PageData();
		try {
			pd = this.getPageData();
			//非空校验
			boolean hasValue = pd.checkParamHasValue(null);
			if(!hasValue){
				geneParasHasNullPd(pd);
				return pd;
			}
			// 获取患者个数                  
			String patientNum = doctorService.getPatientNum(pd);		
			HashMap<String, String> resultMap = new HashMap<String,String>();
			resultMap.put("patientNum", patientNum);
			pd.clear();
			pd.put("errorcode", EnumErrorCode.CODE_000000.code);
			pd.put("errormsg", EnumErrorCode.CODE_000000.msg);
			pd.put("result", "ok");
			// 返回个数
			pd.put("values", resultMap);
		} catch (Exception e) {
			e.printStackTrace();
			pd.clear();
			pd.put("errorcode", EnumErrorCode.CODE_000001.code);
			pd.put("errormsg", e.getMessage());
			pd.put("result", "error");
			pd.put("values", "");
		}
		return pd;
		
		
	}
	
	/**
	 * 
	 * 
	 * @return
	 */
	@RequestMapping(value = "/common/getPushInforMation")
	@ResponseBody
	public PageData getPushInforMation() {
		PageData pd = new PageData();
		try {
			pd = this.getPageData();
			//非空校验
			boolean hasValue = pd.checkParamHasValue(null);
			if(!hasValue){
				geneParasHasNullPd(pd);
				return pd;
			}
			// TODO
//			ServiceResult<List<Map<String,String>>> pushMessages=doctorService.getPushInforMation(pd);
			pd.clear();
			pd.put("errorcode", EnumErrorCode.CODE_000000.code);
			pd.put("errormsg", EnumErrorCode.CODE_000000.msg);
			pd.put("result", "ok");
			// TODO
			pd.put("values", "");
		} catch (Exception e) {
			e.printStackTrace();
			pd.clear();
			pd.put("errorcode", EnumErrorCode.CODE_000001.code);
			pd.put("errormsg", e.getMessage());
			pd.put("result", "error");
			pd.put("values", "");
		}
		return pd;
	}

	
	/**
	 * 8医生手机号搜索获取联系人列表
	 * 
	 * @return
	 */
	@RequestMapping(value = "/doctor/getPlatformUserByPhone")
	@ResponseBody
	public PageData getPlatformUserByPhone() {
		PageData pd = new PageData();
		try {
			pd = this.getPageData();
			//非空校验
			boolean hasValue = pd.checkParamHasValue(null);
			if(!hasValue){
				geneParasHasNullPd(pd);
				return pd;
			}

			ServiceResult<Map<String,String>> platformUserByPhone = doctorService.getPlatformUserByPhone(pd);
			
			pd.clear();
			pd.put("errorcode", platformUserByPhone.getResultCode());
			pd.put("errormsg", platformUserByPhone.getResultMessage());
			pd.put("result", "ok");

			pd.put("values", platformUserByPhone.getResultValue());
		} catch (Exception e) {
			e.printStackTrace();
			pd.clear();
			pd.put("errorcode", EnumErrorCode.CODE_000001.code);
			pd.put("errormsg", e.getMessage());
			pd.put("result", "error");
			pd.put("values", "");
		}
		return pd;
	}
	
	
	/**
	 * 9推送消息时，获取患者标签接口
	 * 
	 * @return
	 */
	@RequestMapping(value = "/common/getPatientTitel")
	@ResponseBody
	public PageData getPatientTitel() {
		PageData pd = new PageData();
		try {
			pd = this.getPageData();
			//非空校验
			boolean hasValue = pd.checkParamHasValue(null);
			if(!hasValue){
				geneParasHasNullPd(pd);
				return pd;
			}
			// TODO
			ServiceResult<List<Map<String,String>>> serviceResult=doctorService.getPatientTitel(pd);
			pd.clear();
			pd.put("errorcode", serviceResult.getResultCode());
			pd.put("errormsg", serviceResult.getResultMessage());
			pd.put("result", serviceResult.getResultMessage());
			// TODO
			pd.put("values", serviceResult.getResultValue());
		} catch (Exception e) {
			e.printStackTrace();
			pd.clear();
			pd.put("errorcode", EnumErrorCode.CODE_000001.code);
			pd.put("errormsg", e.getMessage());
			pd.put("result", "error");
			pd.put("values", "");
		}
		return pd;
	}
	
	
	
	/**
	 * 
	 * 
	 * @return
	 */
	@RequestMapping(value = "/doctor/")
	@ResponseBody
	public PageData test() {
		PageData pd = new PageData();
		try {
			pd = this.getPageData();
			
			// TODO
			
			pd.clear();
			pd.put("errorcode", EnumErrorCode.CODE_000000.code);
			pd.put("errormsg", EnumErrorCode.CODE_000000.msg);
			pd.put("result", "ok");
			// TODO
			pd.put("values", "");
		} catch (Exception e) {
			e.printStackTrace();
			pd.clear();
			pd.put("errorcode", EnumErrorCode.CODE_000001.code);
			pd.put("errormsg", e.getMessage());
			pd.put("result", "error");
			pd.put("values", "");
		}
		return pd;
	}
	
}
