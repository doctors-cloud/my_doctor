package com.liver_cloud.controller.doctor;

import java.io.IOException;
import java.util.List;

import javax.mail.MessagingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSONObject;
import com.liver_cloud.controller.base.BaseController;
import com.liver_cloud.service.doctor.DoctorService;
import com.liver_cloud.service.mail.MailService;
import com.liver_cloud.service.patient.PatientService;
import com.liver_cloud.util.EnumErrorCode;
import com.liver_cloud.util.PageData;
import com.liver_cloud.util.StringUtil;

@Controller
public class DoctorController extends BaseController {

	@Autowired
	private DoctorService doctorService;

	@Autowired
	private PatientService patientService;

	@Autowired
	private MailService mailService;

	/**
	 * 注册医生-获取省市区三级列表
	 * 
	 * @return
	 */
	@RequestMapping(value = "/common/getProvinceCityRegionList")
	@ResponseBody
	public PageData getProvinceCityRegionList() {

		PageData pd = new PageData();
		try {
			pd = this.getPageData();
			List<PageData> provinceCityRegionList = doctorService
					.getProvinceCityRegionList();
			pd.clear();
			pd.put("errorcode", EnumErrorCode.CODE_000000.code);
			pd.put("errormsg", EnumErrorCode.CODE_000000.msg);
			pd.put("result", "ok");
			pd.put("values", provinceCityRegionList);
		} catch (Exception e) {
			// TODO: handle exception
			pd.clear();
			pd.put("errorcode", EnumErrorCode.CODE_000067.code);
			pd.put("errormsg", EnumErrorCode.CODE_000067.msg);
			pd.put("result", "error");
			pd.put("values", "");
		}
		return pd;
	}

	/**
	 * 注册医生基本信息提交接口
	 * 
	 * @return
	 */
	@RequestMapping(value = "/doctor/registerDoctor")
	@ResponseBody
	public PageData registerDoctor() {

		PageData pd = new PageData();
		try {
			pd = this.getPageData();
			String name = pd.getString("name");
			if (name == null || name.equals("")) {
				pd.clear();
				pd.put("errorcode", EnumErrorCode.CODE_000044.code);
				pd.put("errormsg", EnumErrorCode.CODE_000044.msg);
				pd.put("result", "error");
				pd.put("values", "");
				return pd;
			}
			String hospital = pd.getString("hospital");
			if (hospital == null || hospital.equals("")) {
				pd.clear();
				pd.put("errorcode", EnumErrorCode.CODE_000045.code);
				pd.put("errormsg", EnumErrorCode.CODE_000045.msg);
				pd.put("result", "error");
				pd.put("values", "");
				return pd;
			}
			String departmentId = pd.getString("departmentId");
			if (departmentId == null || departmentId.equals("")) {
				pd.clear();
				pd.put("errorcode", EnumErrorCode.CODE_000046.code);
				pd.put("errormsg", EnumErrorCode.CODE_000046.msg);
				pd.put("result", "error");
				pd.put("values", "");
				return pd;
			}
			String professionId = pd.getString("professionId");
			if (professionId == null || professionId.equals("")) {
				pd.clear();
				pd.put("errorcode", EnumErrorCode.CODE_000047.code);
				pd.put("errormsg", EnumErrorCode.CODE_000047.msg);
				pd.put("result", "error");
				pd.put("values", "");
				return pd;
			}
			String province = pd.getString("provinceId");
			if (province == null || province.equals("")) {
				pd.clear();
				pd.put("errorcode", EnumErrorCode.CODE_000048.code);
				pd.put("errormsg", EnumErrorCode.CODE_000048.msg);
				pd.put("result", "error");
				pd.put("values", "");
				return pd;
			}
			String city = pd.getString("cityId");
			if (city == null || city.equals("")) {
				pd.clear();
				pd.put("errorcode", EnumErrorCode.CODE_000052.code);
				pd.put("errormsg", EnumErrorCode.CODE_000052.msg);
				pd.put("result", "error");
				pd.put("values", "");
				return pd;
			}
			String region = pd.getString("regionId");
			if (region == null || region.equals("")) {
				pd.clear();
				pd.put("errorcode", EnumErrorCode.CODE_000049.code);
				pd.put("errormsg", EnumErrorCode.CODE_000049.msg);
				pd.put("result", "error");
				pd.put("values", "");
				return pd;
			}
			String diagnosisType = pd.getString("diagnosisType");
			if (diagnosisType == null || diagnosisType.equals("")) {
				pd.clear();
				pd.put("errorcode", EnumErrorCode.CODE_000051.code);
				pd.put("errormsg", EnumErrorCode.CODE_000051.msg);
				pd.put("result", "error");
				pd.put("values", "");
				return pd;
			}
			String mail = pd.getString("mail");
			if (mail == null || mail.equals("")) {
				pd.clear();
				pd.put("errorcode", EnumErrorCode.CODE_000050.code);
				pd.put("errormsg", EnumErrorCode.CODE_000050.msg);
				pd.put("result", "error");
				pd.put("values", "");
				return pd;
			}
			String mailData = mail.trim();
			boolean ifRight = StringUtil.checkEmail(mailData);
			System.out.println(ifRight);
			if(!ifRight){
				pd.clear();
				pd.put("errorcode", EnumErrorCode.CODE_000055.code);
				pd.put("errormsg", EnumErrorCode.CODE_000055.msg);
				pd.put("result", "error");
				pd.put("values", "");
				return pd;
			}
			pd.put("mail", mailData);
			PageData doctorInfo = doctorService.saveDoctorInfo(pd);
			pd.clear();
			if (doctorInfo == null) {
				pd.put("errorcode", EnumErrorCode.CODE_000037.code);
				pd.put("errormsg", EnumErrorCode.CODE_000037.msg);
				pd.put("result", "error");
				pd.put("values", "");
				return pd;
			}
			pd.put("errorcode", EnumErrorCode.CODE_000000.code);
			pd.put("errormsg", EnumErrorCode.CODE_000000.msg);
			pd.put("result", "ok");
			pd.put("values", doctorInfo);
		} catch (Exception e) {
			// TODO: handle exception
			pd.clear();
			pd.put("errorcode", EnumErrorCode.CODE_000027.code);
			pd.put("errormsg", EnumErrorCode.CODE_000027.msg);
			pd.put("result", "error");
			pd.put("values", "");
		}
		return pd;
	}

	/**
	 * 医生上传医师资格证书
	 * 
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/doctor/uploadCertificate")
	@ResponseBody
	public PageData uploadCertificate(HttpServletRequest request,
			HttpServletResponse response) {

		PageData pd = new PageData();
		try {
			pd = this.getPageData();
			PageData certificateUrl = doctorService.saveCertificate(request,
					response);
			pd.clear();
			pd.put("errorcode", EnumErrorCode.CODE_000000.code);
			pd.put("errormsg", EnumErrorCode.CODE_000000.msg);
			pd.put("result", "ok");
			pd.put("values", certificateUrl);
		} catch (Exception e) {
			// TODO: handle exception
			pd.clear();
			pd.put("errorcode", EnumErrorCode.CODE_000028.code);
			pd.put("errormsg", EnumErrorCode.CODE_000028.msg);
			pd.put("result", "error");
			pd.put("values", "");
		}
		return pd;
	}

	/**
	 * 医师身份审核接口
	 * 
	 * @return
	 */
	@RequestMapping(value = "/doctor/checkCertificate")
	@ResponseBody
	public PageData checkCertificate() {

		PageData pd = new PageData();
		try {
			pd = this.getPageData();
			PageData checkState = doctorService.checkCertificate(pd);
			pd.clear();
			pd.put("errorcode", EnumErrorCode.CODE_000000.code);
			pd.put("errormsg", EnumErrorCode.CODE_000000.msg);
			pd.put("result", "ok");
			pd.put("values", checkState);
		} catch (Exception e) {
			// TODO: handle exception
			pd.clear();
			pd.put("errorcode", EnumErrorCode.CODE_000017.code);
			pd.put("errormsg", EnumErrorCode.CODE_000017.msg);
			pd.put("result", "error");
			pd.put("values", "");
		}
		return pd;
	}

	/**
	 * 医生获取信息列表接口
	 * 
	 * @return
	 */
	@RequestMapping(value = "/doctor/getDoctorMessage")
	@ResponseBody
	public PageData getDoctorMessage() {

		PageData pd = new PageData();
		try {
			pd = this.getPageData();
			System.out.println(pd);
			PageData doctorMessage = doctorService.getDoctorMessage(pd);
			pd.clear();
			pd.put("errorcode", EnumErrorCode.CODE_000000.code);
			pd.put("errormsg", EnumErrorCode.CODE_000000.msg);
			pd.put("result", "ok");
			pd.put("values", doctorMessage);
		} catch (Exception e) {
			// TODO: handle exception
			pd.clear();
			pd.put("errorcode", EnumErrorCode.CODE_000018.code);
			pd.put("errormsg", EnumErrorCode.CODE_000018.msg);
			pd.put("result", "error");
			pd.put("values", "");
		}
		return pd;
	}

	/**
	 * 医生获取患者详情接口,将该回话对应的doctorchanged改为0
	 * 
	 * @return
	 * @throws Exception 
	 */
	@RequestMapping(value = "/doctor/getPatientQuestionDetail")
	@ResponseBody
	public PageData getPatientQuestionDetail() throws Exception {

		PageData pd = new PageData();
		try {
			pd = this.getPageData();
			PageData questionDetail = doctorService
					.updateAndgetPatientQuestionDetail(pd);
			pd.clear();
			pd.put("errorcode", EnumErrorCode.CODE_000000.code);
			pd.put("errormsg", EnumErrorCode.CODE_000000.msg);
			pd.put("result", "ok");
			pd.put("values", questionDetail);
		} catch (Exception e) {
			// TODO: handle exception
			pd.clear();
			pd.put("errorcode", EnumErrorCode.CODE_000019.code);
			pd.put("errormsg", EnumErrorCode.CODE_000019.msg);
			pd.put("result", "error");
			pd.put("values", "");
		}
		return pd;
	}

	/**
	 * 医生获取患者病例接口
	 * 
	 * @return
	 */
	@RequestMapping(value = "/doctor/getPatientCase")
	@ResponseBody
	public PageData getPatientCase() {

		PageData pd = new PageData();
		try {
			pd = this.getPageData();
			// PageData patientCase = doctorService.getPatientCase(pd);
			PageData patientCase = patientService.getPatientCasePage(pd);
			pd.clear();
			pd.put("errorcode", EnumErrorCode.CODE_000000.code);
			pd.put("errormsg", EnumErrorCode.CODE_000000.msg);
			pd.put("result", "ok");
			pd.put("values", patientCase);
		} catch (Exception e) {
			// TODO: handle exception
			pd.clear();
			pd.put("errorcode", EnumErrorCode.CODE_000021.code);
			pd.put("errormsg", EnumErrorCode.CODE_000021.msg);
			pd.put("result", "error");
			pd.put("values", "");
		}
		return pd;
	}

	/**
	 * 医生回复患者问题
	 * 
	 * @return
	 */
	@RequestMapping(value = "/doctor/replyPatient")
	@ResponseBody
	public PageData replyPatient() {

		PageData pd = new PageData();
		try {
			pd = this.getPageData();
			doctorService.saveDoctorQuestion(pd);
			pd.clear();
			pd.put("errorcode", EnumErrorCode.CODE_000000.code);
			pd.put("errormsg", EnumErrorCode.CODE_000000.msg);
			pd.put("result", "ok");
			pd.put("values", "ok");
		} catch (Exception e) {
			// TODO: handle exception
			pd.clear();
			pd.put("errorcode", EnumErrorCode.CODE_000029.code);
			pd.put("errormsg", EnumErrorCode.CODE_000029.msg);
			pd.put("result", "error");
			pd.put("values", "");
		}
		return pd;
	}

	/**
	 * 医生保存方案
	 * 
	 * @return
	 * @throws Exception 
	 */
	@RequestMapping(value = "/doctor/uploadSolution")
	@ResponseBody
	public PageData uploadSolution() throws Exception {

		PageData pd = new PageData();
		try {
			pd = this.getPageData();
			doctorService.saveSolution(pd);
			pd.clear();
			pd.put("errorcode", EnumErrorCode.CODE_000000.code);
			pd.put("errormsg", EnumErrorCode.CODE_000000.msg);
			pd.put("result", "ok");
			pd.put("values", "ok");
		} catch (Exception e) {
			// TODO: handle exception
			pd.clear();
			pd.put("errorcode", EnumErrorCode.CODE_000020.code);
			pd.put("errormsg", EnumErrorCode.CODE_000020.msg);
			pd.put("result", "error");
			pd.put("values", "");
		}
		return pd;
	}

	/**
	 * 医生获取我的患者病症列表接口
	 * 
	 * @return
	 */
	@RequestMapping(value = "/doctor/getPatientDiagnosisList")
	@ResponseBody
	public PageData getPatientDiagnosisList() {

		PageData pd = new PageData();
		try {
			pd = this.getPageData();
			List<String> patientDiagnosisList = doctorService
					.getPatientDiagnosisList(pd);
			pd.clear();
			pd.put("errorcode", EnumErrorCode.CODE_000000.code);
			pd.put("errormsg", EnumErrorCode.CODE_000000.msg);
			pd.put("result", "ok");
			pd.put("values", patientDiagnosisList);
		} catch (Exception e) {
			// TODO: handle exception
			pd.clear();
			pd.put("errorcode", EnumErrorCode.CODE_000033.code);
			pd.put("errormsg", EnumErrorCode.CODE_000033.msg);
			pd.put("result", "error");
			pd.put("values", "");
		}
		return pd;
	}

	/**
	 * 医生获取患者列表接口
	 * 
	 * @return
	 */
	@RequestMapping(value = "/doctor/getPatientList")
	@ResponseBody
	public PageData getPatientList() {

		PageData pd = new PageData();
		try {

			pd = this.getPageData();
			List<PageData> patientList = doctorService.getPatientList(pd);
			pd.clear();
			pd.put("errorcode", EnumErrorCode.CODE_000000.code);
			pd.put("errormsg", EnumErrorCode.CODE_000000.msg);
			pd.put("result", "ok");
			pd.put("values", patientList);
		} catch (Exception e) {
			// TODO: handle exception
			pd.clear();
			pd.put("errorcode", EnumErrorCode.CODE_000022.code);
			pd.put("errormsg", EnumErrorCode.CODE_000022.msg);
			pd.put("result", "error");
			pd.put("values", "");
		}
		return pd;
	}

	/**
	 * 医生转诊病人接口
	 * 
	 * @return
	 * @throws Exception 
	 */
	@RequestMapping(value = "/doctor/sendPatientTransferMail" )
	@ResponseBody
	public PageData sendPatientTransferMail(HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		PageData pd = new PageData();
		try {
			JSONObject param = StringUtil.getJSONObject(request);
			//验证邮箱格式是否正确
			JSONObject paramContent = param.getJSONObject("param");
			String mailTo = paramContent.getString("mailTo");
			boolean ifRight = StringUtil.checkEmail(mailTo);
			if(!ifRight){
				pd.clear();
				pd.put("errorcode", EnumErrorCode.CODE_000055.code);
				pd.put("errormsg", EnumErrorCode.CODE_000055.msg);
				pd.put("result", "error");
				pd.put("values", "");
				return pd;
			}
			doctorService.saveAndsendTransferMail(param);
			pd.clear();
			pd.put("errorcode", EnumErrorCode.CODE_000000.code);
			pd.put("errormsg", EnumErrorCode.CODE_000000.msg);
			pd.put("result", "ok");
			pd.put("values", "ok");
	} catch (Exception e) {
			// TODO: handle exception
			pd.clear();
			pd.put("errorcode", EnumErrorCode.CODE_000036.code);
			pd.put("errormsg", EnumErrorCode.CODE_000036.msg);
			pd.put("result", "error");
			pd.put("values", "");
		}

		return pd;
	}

	/**
	 * 获取转诊验证码接口
	 * 
	 * @return
	 */
	@RequestMapping(value = "/doctor/getTransferCode")
	@ResponseBody
	public PageData getTransferCode() {
		PageData pd = new PageData();
		try {
			PageData code = doctorService.getTransferCode();
			pd.clear();
			pd.put("errorcode", EnumErrorCode.CODE_000000.code);
			pd.put("errormsg", EnumErrorCode.CODE_000000.msg);
			pd.put("result", "ok");
			pd.put("values", code);

		} catch (Exception e) {
			// TODO: handle exception
			pd.clear();
			pd.put("errorcode", EnumErrorCode.CODE_000039.code);
			pd.put("errormsg", EnumErrorCode.CODE_000039.msg);
			pd.put("result", "error");
			pd.put("values", "");
		}
		return pd;
	}

	/**
	 * 医生是否接受转诊患者接口
	 * 
	 * @return
	 * @throws Exception 
	 */
	@RequestMapping(value = "/doctor/adoptTransferPatient")
	@ResponseBody
	public PageData adoptTransferPatient() throws Exception {
		PageData pd = new PageData();
		try {
			System.out.println("jinru");
			pd=this.getPageData();
			doctorService.adoptTransferPatient(pd);
			pd.clear();
			pd.put("errorcode", EnumErrorCode.CODE_000000.code);
			pd.put("errormsg", EnumErrorCode.CODE_000000.msg);
			pd.put("result", "ok");
			pd.put("values", "ok");

		} catch (Exception e) {
			// TODO: handle exception
			pd.clear();
			pd.put("errorcode", EnumErrorCode.CODE_000041.code);
			pd.put("errormsg", EnumErrorCode.CODE_000041.msg);
			pd.put("result", "error");
			pd.put("values", "");
		}
		return pd;

	}

	/**
	 * 获取医师资格证书图片示例接口
	 * 
	 * @return
	 */
	@RequestMapping(value = "/doctor/getCertificateUrlList")
	@ResponseBody
	public PageData getCertificateUrlList() {
		PageData pd = new PageData();

		try {
			List<PageData> sampleUrlList = doctorService
					.getCertificateUrlList();
			pd.clear();
			pd.put("errorcode", EnumErrorCode.CODE_000000.code);
			pd.put("errormsg", EnumErrorCode.CODE_000000.msg);
			pd.put("result", "ok");
			pd.put("values", sampleUrlList);
		} catch (Exception e) {
			// TODO: handle exception
			pd.clear();
			pd.put("errorcode", EnumErrorCode.CODE_000053.code);
			pd.put("errormsg", EnumErrorCode.CODE_000053.msg);
			pd.put("result", "error");
			pd.put("values", "");
		}

		return pd;
	}
	
	
	
	/**
	 * 删除医师资格证书接口
	 * @return
	 */
	@RequestMapping(value="/doctor/deleteCertificate")
	@ResponseBody
	public PageData deleteCertificate(){
		PageData pd = new PageData();
		try {
			doctorService.deleteCertificate(pd);
			pd.clear();
			pd.put("errorcode", EnumErrorCode.CODE_000000.code);
			pd.put("errormsg", EnumErrorCode.CODE_000000.msg);
			pd.put("result", "ok");
			pd.put("values", "ok");

		} catch (Exception e) {
			// TODO: handle exception
			pd.clear();
			pd.put("errorcode", EnumErrorCode.CODE_000054.code);
			pd.put("errormsg", EnumErrorCode.CODE_000054.msg);
			pd.put("result", "error");
			pd.put("values", "");
		}
		return pd;

	}
	
	
	/**
	 * 记录医生是否跳转过通过审核页面
	 * @return
	 */
	@RequestMapping(value="/doctor/recordPassThrough")
	@ResponseBody
	public PageData recordPassThrough(){
		PageData pd = new PageData();
		try {
			pd = this.getPageData();
			doctorService.savePassThrough(pd);
			pd.clear();
			pd.put("errorcode", EnumErrorCode.CODE_000000.code);
			pd.put("errormsg", EnumErrorCode.CODE_000000.msg);
			pd.put("result", "ok");
			pd.put("values", "ok");

		} catch (Exception e) {
			// TODO: handle exception
			pd.clear();
			pd.put("errorcode", EnumErrorCode.CODE_000056.code);
			pd.put("errormsg", EnumErrorCode.CODE_000056.msg);
			pd.put("result", "error");
			pd.put("values", "");
		}
		return pd;

	}
	
	
	/**
	 * 医生信息状态是否更改接口
	 * @return
	 */
	@RequestMapping(value="/doctor/getDoctorMessageChanged")
	@ResponseBody
	public PageData getDoctorMessageChanged(){
		
		PageData pd = new PageData();		
	try {
			pd = this.getPageData();
			PageData messageChanged = doctorService.getDoctorMessageChanged(pd);
			pd.clear();
			pd.put("errorcode",  EnumErrorCode.CODE_000000.code);
			pd.put("errormsg", EnumErrorCode.CODE_000000.msg);
			pd.put("result", "ok");
			pd.put("values", messageChanged);
		} catch (Exception e) {
			// TODO: handle exception
			pd.clear();
			pd.put("errorcode",  EnumErrorCode.CODE_000058.code);
			pd.put("errormsg", EnumErrorCode.CODE_000058.msg);
			pd.put("result", "error");
			pd.put("values", "");
		}		
		return pd;	
	}
	
	
	/**
	 * 获取联想医院列表
	 * @return
	 */
	@RequestMapping(value="/doctor/getHospitalList")
	@ResponseBody
	public PageData getHospitalList(){
		
		PageData pd = new PageData();		
	try {
			pd = this.getPageData();
			PageData hospitalList = doctorService.getHospitalList(pd);
			pd.clear();
			pd.put("errorcode",  EnumErrorCode.CODE_000000.code);
			pd.put("errormsg", EnumErrorCode.CODE_000000.msg);
			pd.put("result", "ok");
			pd.put("values", hospitalList);
		} catch (Exception e) {
			// TODO: handle exception
			pd.clear();
			pd.put("errorcode",  EnumErrorCode.CODE_000061.code);
			pd.put("errormsg", EnumErrorCode.CODE_000061.msg);
			pd.put("result", "error");
			pd.put("values", "");
		}		
		return pd;	
	}
	
	/**
	 * 医生获取助手列表
	 * @Description:TODO
	 * @return
	 * @author: lifutao
	 * @time:2017年4月25日 下午3:39:46
	 */
	@RequestMapping(value="/doctor/getAssistantList")
	@ResponseBody
	public PageData getAssistantList(){
		
		PageData pd = new PageData();		
	try {
			pd = this.getPageData();
			List<PageData> assistantList = doctorService.getAssistantList(pd);
			pd.clear();
			pd.put("errorcode",  EnumErrorCode.CODE_000000.code);
			pd.put("errormsg", EnumErrorCode.CODE_000000.msg);
			pd.put("result", "ok");
			pd.put("values", assistantList);
		} catch (Exception e) {
			// TODO: handle exception
			pd.clear();
			pd.put("errorcode",  EnumErrorCode.CODE_000074.code);
			pd.put("errormsg", EnumErrorCode.CODE_000074.msg);
			pd.put("result", "error");
			pd.put("values", "");
		}		
		return pd;	
	}
	
	/**
	 * 医生查看助手回答的问题
	 * @Description:TODO
	 * @return
	 * @author: lifutao
	 * @time:2017年4月25日 下午5:51:08
	 */
	@RequestMapping(value="/doctor/getAssistantAnswerList")
	@ResponseBody
	public PageData getAssistantAnswerList(){
		
		PageData pd = new PageData();		
	try {
			pd = this.getPageData();
			List<PageData> answerList = doctorService.getAssistantAnswerList(pd);
			pd.clear();
			pd.put("errorcode",  EnumErrorCode.CODE_000000.code);
			pd.put("errormsg", EnumErrorCode.CODE_000000.msg);
			pd.put("result", "ok");
			pd.put("values", answerList);
		} catch (Exception e) {
			// TODO: handle exception
			pd.clear();
			pd.put("errorcode",  EnumErrorCode.CODE_000075.code);
			pd.put("errormsg", EnumErrorCode.CODE_000075.msg);
			pd.put("result", "error");
			pd.put("values", "");
		}		
		return pd;	
	}
	
	
	/**
	 * 医生发送短信邀请助手,后台发短信接口
	 * @Description:TODO
	 * @return
	 * @author: lifutao
	 * @time:2017年4月26日 下午4:04:51
	 */
	@RequestMapping(value="/doctor/inviteAssistantByPhoneNum")
	@ResponseBody
	public PageData inviteAssistantByPhoneNum(){
		
		PageData pd = new PageData();		
	try {
			pd = this.getPageData();
			doctorService.sendMessage(pd);
			pd.clear();
			pd.put("errorcode",  EnumErrorCode.CODE_000000.code);
			pd.put("errormsg", EnumErrorCode.CODE_000000.msg);
			pd.put("result", "ok");
			pd.put("values", "ok");
		} catch (Exception e) {
			// TODO: handle exception
			pd.clear();
			pd.put("errorcode",  EnumErrorCode.CODE_000076.code);
			pd.put("errormsg", EnumErrorCode.CODE_000076.msg);
			pd.put("result", "error");
			pd.put("values", "");
		}		
		return pd;	
	}
	
	
	/**
	 * 医生获取已分配及未分配患者列表接口
	 * @Description:TODO
	 * @return
	 * @author: lifutao
	 * @time:2017年4月26日 下午4:56:56
	 */
	@RequestMapping(value="/doctor/getAllotPatientList")
	@ResponseBody
	public PageData getAllotPatientList(){
		
		PageData pd = new PageData();		
	try {
			pd = this.getPageData();
			PageData allotPatientInfo =  doctorService.getAllotPatientList(pd);
			pd.clear();
			pd.put("errorcode",  EnumErrorCode.CODE_000000.code);
			pd.put("errormsg", EnumErrorCode.CODE_000000.msg);
			pd.put("result", "ok");
			pd.put("values", allotPatientInfo);
		} catch (Exception e) {
			// TODO: handle exception
			pd.clear();
			pd.put("errorcode",  EnumErrorCode.CODE_000077.code);
			pd.put("errormsg", EnumErrorCode.CODE_000077.msg);
			pd.put("result", "error");
			pd.put("values", "");
		}		
		return pd;	
	}
	
	/**
	 * 在已分配或未分配界面，输入患者姓名，手机号搜索病人接口
	 * @Description:TODO
	 * @return
	 * @author: lifutao
	 * @time:2017年4月27日 下午1:47:57
	 */
	@RequestMapping(value="/doctor/searchAllotPatient")
	@ResponseBody
	public PageData searchAllotPatient(){
		
		PageData pd = new PageData();		
	try {
			pd = this.getPageData();
			List<PageData> patientList =  doctorService.searchAllotPatient(pd);
			pd.clear();
			pd.put("errorcode",  EnumErrorCode.CODE_000000.code);
			pd.put("errormsg", EnumErrorCode.CODE_000000.msg);
			pd.put("result", "ok");
			pd.put("values", patientList);
		} catch (Exception e) {
			// TODO: handle exception
			pd.clear();
			pd.put("errorcode",  EnumErrorCode.CODE_000078.code);
			pd.put("errormsg", EnumErrorCode.CODE_000078.msg);
			pd.put("result", "error");
			pd.put("values", "");
		}		
		return pd;	
	}
	
	/**
	 * 医生给病人添加标签
	 * @Description:TODO
	 * @return
	 * @author: lifutao
	 * @time:2017年4月27日 下午3:03:08
	 */
	@RequestMapping(value="/doctor/addPatientTitel")
	@ResponseBody
	public PageData addPatientTitel(){
		
		PageData pd = new PageData();		
	try {
			pd = this.getPageData();
			doctorService.savePatientTitel(pd);
			pd.clear();
			pd.put("errorcode",  EnumErrorCode.CODE_000000.code);
			pd.put("errormsg", EnumErrorCode.CODE_000000.msg);
			pd.put("result", "ok");
			pd.put("values", "ok");
		} catch (Exception e) {
			// TODO: handle exception
			pd.clear();
			pd.put("errorcode",  EnumErrorCode.CODE_000079.code);
			pd.put("errormsg", EnumErrorCode.CODE_000079.msg);
			pd.put("result", "error");
			pd.put("values", "");
		}		
		return pd;	
	}
	
	
	/**
	 * 医生编辑患者标签时，获取医生已经自定义的标签
	 * @Description:TODO
	 * @return
	 * @author: lifutao
	 * @time:2017年4月27日 下午4:20:59
	 */
	@RequestMapping(value="/doctor/getDefinePatientTitel")
	@ResponseBody
	public PageData getDefinePatientTitel(){
		
		PageData pd = new PageData();		
	try {
			pd = this.getPageData();
			List<PageData> patientTitelList = doctorService.getDefinePatientTitel(pd);
			pd.clear();
			pd.put("errorcode",  EnumErrorCode.CODE_000000.code);
			pd.put("errormsg", EnumErrorCode.CODE_000000.msg);
			pd.put("result", "ok");
			pd.put("values", patientTitelList);
		} catch (Exception e) {
			// TODO: handle exception
			pd.clear();
			pd.put("errorcode",  EnumErrorCode.CODE_000080.code);
			pd.put("errormsg", EnumErrorCode.CODE_000080.msg);
			pd.put("result", "error");
			pd.put("values", "");
		}		
		return pd;	
	}
	
	/**
	 * 医生将患者分配给助手
	 * @Description:TODO
	 * @return
	 * @author: lifutao
	 * @time:2017年4月27日 下午4:42:38
	 */
	@RequestMapping(value="/doctor/distributePatientToAssistant")
	@ResponseBody
	public PageData distributePatientToAssistant(){
		
		PageData pd = new PageData();		
	try {
			pd = this.getPageData();
			doctorService.saveAndDistributePatient(pd);
			pd.clear();
			pd.put("errorcode",  EnumErrorCode.CODE_000000.code);
			pd.put("errormsg", EnumErrorCode.CODE_000000.msg);
			pd.put("result", "ok");
			pd.put("values", "ok");
		} catch (Exception e) {
			// TODO: handle exception
			pd.clear();
			pd.put("errorcode",  EnumErrorCode.CODE_000081.code);
			pd.put("errormsg", EnumErrorCode.CODE_000081.msg);
			pd.put("result", "error");
			pd.put("values", "");
		}		
		return pd;	
	}
	
}
