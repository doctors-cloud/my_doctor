package com.liver_cloud.controller.user;

import java.io.BufferedReader;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.liver_cloud.controller.base.BaseController;
import com.liver_cloud.service.UserService;
import com.liver_cloud.util.PageData;

@Controller
public class UserController extends BaseController{
		
	@Autowired
	private UserService  userService;
	
	
	@RequestMapping(value="/getIn" )
	@ResponseBody
	public PageData getIn(HttpServletRequest request, HttpServletResponse response) throws Exception{
		
		PageData pd = new PageData();
		StringBuilder buffer = new StringBuilder();  
		BufferedReader reader = request.getReader();  
		String line;  
		 while ((line = reader.readLine()) != null) {  
		    buffer.append(line);  
		 }  
		String body = buffer.toString(); 
		System.out.println(body);
//		try {
			pd = this.getPageData();
			System.out.println(pd);
			//userService.getIn(pd);
			pd.clear();
			pd.put("result", "ok");
			return pd;
			
	/*	} catch (Exception e) {
			// TODO: handle exception
			pd.clear();
			pd.put("result", "error");
			return pd;
		}*/
		
	}
	
}
