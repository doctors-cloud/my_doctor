package com.liver_cloud.model;

import com.liver_cloud.util.EnumErrorCode;

public class ServiceResult<T> {
	
	private String resultCode;
	private String resultMessage;
	private T resultValue;
	private EnumErrorCode resultEnum;
	
	public String getResultCode() {
		return resultCode;
	}
	public void setResultCode(String resultCode) {
		this.resultCode = resultCode;
	}
	public String getResultMessage() {
		return resultMessage;
	}
	public void setResultMessage(String resultMessage) {
		this.resultMessage = resultMessage;
	}
	public T getResultValue() {
		return resultValue;
	}
	public void setResultValue(T result) {
		this.resultValue = result;
	}

	public EnumErrorCode getResultEnum() {
		return resultEnum;
	}
	public void setResultEnum(EnumErrorCode resultEnum) {
		this.resultEnum = resultEnum;
		this.resultCode=resultEnum.code;

	} 
}
