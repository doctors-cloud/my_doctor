package com.liver_cloud.bean;

import java.util.Date;
import java.util.List;

/**
 * 推送消息
 * @author yajun
 * 2017年4月24日
 */
public class PushMessage {
	
	public final static Integer pushTypeDoctor=0;
	public final static Integer pushTypePatient=1;
	
	private Integer pmId;

    private Byte pmType;

    private Date pmDate;

    private Integer pmUserId;

    private String pmTitle;

    private Byte pmIfChanged;

    private String pmContent;

	/**
	 * 推送图片
	 */
	private List<String> imageUrl;
	
	/**
	 * 已读者列表
	 */
	private List<Integer> readerUserIdList;
	
	/**
	 *  点赞者列表 
	 */
	private List<Integer> supportUserIdList;
	
	public Integer getPmId() {
        return pmId;
    }

    public void setPmId(Integer pmId) {
        this.pmId = pmId;
    }

    public Byte getPmType() {
        return pmType;
    }

    public void setPmType(Byte pmType) {
        this.pmType = pmType;
    }

    public Date getPmDate() {
        return pmDate;
    }

    public void setPmDate(Date pmDate) {
        this.pmDate = pmDate;
    }

    public Integer getPmUserId() {
        return pmUserId;
    }

    public void setPmUserId(Integer pmUserId) {
        this.pmUserId = pmUserId;
    }

    public String getPmTitle() {
        return pmTitle;
    }

    public void setPmTitle(String pmTitle) {
        this.pmTitle = pmTitle == null ? null : pmTitle.trim();
    }

    public Byte getPmIfChanged() {
        return pmIfChanged;
    }

    public void setPmIfChanged(Byte pmIfChanged) {
        this.pmIfChanged = pmIfChanged;
    }

    public String getPmContent() {
        return pmContent;
    }

    public void setPmContent(String pmContent) {
        this.pmContent = pmContent == null ? null : pmContent.trim();
    }
	

	public List<String> getImageUrl() {
		return imageUrl;
	}

	public void setImageUrl(List<String> imageUrl) {
		this.imageUrl = imageUrl;
	}

	public List<Integer> getReaderUserIdList() {
		return readerUserIdList;
	}

	public void setReaderUserIdList(List<Integer> readerUserIdList) {
		this.readerUserIdList = readerUserIdList;
	}

	public List<Integer> getSupportUserIdList() {
		return supportUserIdList;
	}

	public void setSupportUserIdList(List<Integer> supportUserIdList) {
		this.supportUserIdList = supportUserIdList;
	}

	@Override
	public String toString() {
		return "PushMessage [pmId=" + pmId + ", pmType=" + pmType + ", pmDate=" + pmDate + ", pmUserId=" + pmUserId
				+ ", pmTitle=" + pmTitle + ", pmIfChanged=" + pmIfChanged + ", pmContent=" + pmContent + ", imageUrl="
				+ imageUrl + ", readerUserIdList=" + readerUserIdList + ", supportUserIdList=" + supportUserIdList
				+ "]";
	}
	
	
}
