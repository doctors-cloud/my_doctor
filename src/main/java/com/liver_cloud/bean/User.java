package com.liver_cloud.bean;

/**
 * 注册系统的所有用户
 * @author yajun
 * 2017年4月24日
 */
public class User {

	private Integer userId;
	
	private String nickName;
	
	/**
	 * 用户头像
	 */
	private String headPortrait;
	
	/**
	 * 用户手机号（数据库中为mobile）
	 */
	private String phoneNumber;

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public String getNickName() {
		return nickName;
	}

	public void setNickName(String nickName) {
		this.nickName = nickName;
	}

	public String getHeadPortrait() {
		return headPortrait;
	}

	public void setHeadPortrait(String headPortrait) {
		this.headPortrait = headPortrait;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	
	
}
