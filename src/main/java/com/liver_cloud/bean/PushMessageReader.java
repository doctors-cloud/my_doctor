package com.liver_cloud.bean;

public class PushMessageReader {
    private Integer pmrId;

    private Byte pmrReadType;

    private Integer pmrReaderId;

    private Integer pmrPushMessageId;

    public Integer getPmrId() {
        return pmrId;
    }

    public void setPmrId(Integer pmrId) {
        this.pmrId = pmrId;
    }

    public Byte getPmrReadType() {
        return pmrReadType;
    }

    public void setPmrReadType(Byte pmrReadType) {
        this.pmrReadType = pmrReadType;
    }

    public Integer getPmrReaderId() {
        return pmrReaderId;
    }

    public void setPmrReaderId(Integer pmrReaderId) {
        this.pmrReaderId = pmrReaderId;
    }

    public Integer getPmrPushMessageId() {
        return pmrPushMessageId;
    }

    public void setPmrPushMessageId(Integer pmrPushMessageId) {
        this.pmrPushMessageId = pmrPushMessageId;
    }
}