package com.liver_cloud.dao;

import java.util.List;
import java.util.Map;

import com.liver_cloud.bean.PushMessage;

public interface PushMessageMapper {
    int deleteByPrimaryKey(Integer pmId);

    int insert(PushMessage record);

    int insertSelective(PushMessage record);

    PushMessage selectByPrimaryKey(Integer pmId);

    int updateByPrimaryKeySelective(PushMessage record);

    int updateByPrimaryKeyWithBLOBs(PushMessage record);

    int updateByPrimaryKey(PushMessage record);

	List<PushMessage> selectByPrimaryKeyList(Map<String,Object> params);
}