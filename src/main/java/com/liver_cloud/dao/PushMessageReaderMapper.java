package com.liver_cloud.dao;

import java.util.List;

import com.liver_cloud.bean.PushMessageReader;

public interface PushMessageReaderMapper {
    int deleteByPrimaryKey(Integer pmrId);

    int insert(PushMessageReader record);

    int insertSelective(PushMessageReader record);

    PushMessageReader selectByPrimaryKey(Integer pmrId);

    int updateByPrimaryKeySelective(PushMessageReader record);

    int updateByPrimaryKey(PushMessageReader record);
    
    List<Integer> selectByReaderUserId(Integer readerUserId);
}