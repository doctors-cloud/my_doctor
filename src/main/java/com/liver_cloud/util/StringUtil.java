package com.liver_cloud.util;

import java.io.BufferedReader;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.ServletInputStream;
import javax.servlet.http.HttpServletRequest;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;

public class StringUtil {

	public static JSONObject getJSONObject(HttpServletRequest request)
			throws IOException {
		StringBuilder buffer = new StringBuilder();		
		BufferedReader reader = request.getReader();
		String line;
		while ((line = reader.readLine()) != null) {
			buffer.append(line);
		}
		String body = buffer.toString();
		System.out.println(body+"body");
		JSONObject obj = new JSONObject();
		JSONObject param = obj.parseObject(body);
		return param;
	}

	public static String createHtmlTxt (String code,String patientName,boolean ifRegisted,
			String doctorId,String patientId,List<String> content){
		
		StringBuilder builder = new StringBuilder();
		String codeString = "<tr>"+
                "<td style=\"padding:25px 0;font-size:28px;\">注册邀请码："+code+"</td>"+
            "</tr>"; 
		String myDoctorId = doctorId;
		List<String> mailContent = content;
		String myPatientId = patientId;
		String adoptUrl = "http://camp.liver-cloud.com/my_doctor/project2/accept.html?"+"doctorId="+myDoctorId+"&patientId="+myPatientId+"&code="+code+"&adopt=1";//adopt=1代表接受,0代表拒绝
		//String adoptUrl = "http://10.0.1.2:8080/my_doctor/project2/accept.html?"+"doctorId="+myDoctorId+"&patientId="+myPatientId+"&code="+code+"&adopt=1";//adopt=1代表接受,0代表拒绝
		String abandonUrl = "http://camp.liver-cloud.com/my_doctor/project2/refused.html?"+"doctorId="+myDoctorId+"&patientId="+myPatientId+"&code="+code+"&adopt=0";
		builder.
		append("<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">");
		builder.
		append("<html xmlns=\"http://www.w3.org/1999/xhtml\">");
		builder.
		append("<head>")
		.append(" <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\" />");
		builder.
		append(" <title>HTML Email</title>").
		append(" <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\" />");
		builder.
		append("<style>").
		append(" img {"+
            "outline: none;"+
            "text-decoration: none;"+
            "-ms-interpolation-mode: bicubic;"+
       "}"
        +
        "a img {" +
           " border: none;"+
       " }");
		builder.
		append("<img border=\"0\" style=\"display:block;\">").
		append("</style>").
		append("</head>").
		append("<body style=\"margin: 0; padding: 0;\">").
		append("<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\">").
		append("<table align=\"center\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"720\" style=\"border-collapse: collapse; background:url('./images/background.png');background-repeat: no-repeat;").
		append(" background-size: contain;\">").
		append(" <tr> "+
               "<td style=\"padding:25px 0;font-size:28px;\"> 你好， </td>"+
            "</tr>"+
            "<tr>"+
            "<td style=\"padding:10px 0;font-size:28px;\">针对于患者"+patientName+"的在您领域的一些专业性问题，希望您能协同帮助解答,具体问题如下。</td> </tr>");  

		for(int i = 0;i<mailContent.size();i++){
			System.out.println(mailContent.get(i));
			String contentTemp = "<tr> <td style=\"padding:10px 0;font-size:28px;\">"+mailContent.get(i)+"。</td> </tr>";
        	builder.append(contentTemp);
        }   
		if(!ifRegisted){
			builder.append(codeString);
		}
        builder.append("<tr>"+
                "<td style=\"text-align:center\"><a style=\"display: block;").
        append("height: 70px;"+
        		"width: 600px;"+
				"background-color: #3BC4E6;"+
				"border-radius: 10px;"+
				"padding-top:23px;"+
				"color: #fff;margin:30px auto 0 auto;font-size:28px; text-decoration:none;\" href=\""+adoptUrl+"\">前去回答问题</a></td>").
	    append("  </tr>"+
            "<tr>"+
                "<td style=\"text-align:center\"><a style=\"display: block;").
        append(" height: 70px;"+
        		"width: 600px;" +
        		"border-radius: 10px;"+
        		"border:1px solid #3BC4E6;"+
        		"padding-top: 23px;"+
        		"color: #3BC4E6;margin:26px auto;font-size:28px; text-decoration:none;\">拒绝回答问题</a></td>").
        append(" </tr>"+
           " <tr>"+ 
           "<td style=\"text-align:center;\"><img src=\"http://camp.liver-cloud.com/mail/mail.png\" alt=\"liver-care\" style=\"margin-top:92px;\">"+
                    "<p style=\"color:#707070\">前去下载liver-care</p>"+
                "</td>"+
            "</tr>"+
        "</table>"+
        "</table>"+
        "</body>"+
        "</html>");
		String htmlTxt = builder.toString();		
		return htmlTxt;
		
	}

	public static List<String> createContent(String patientName,
			JSONArray templateList) {
		// TODO Auto-generated method stub

		List<String> list = new ArrayList<String>();
		for(int i= 0;i<templateList.size();i++){
			 StringBuilder builder = new StringBuilder();
			 JSONObject objTemp = templateList.getJSONObject(i);
			 String templateId  = objTemp.get("templateId").toString();
			 String templateContent = objTemp.get("templateContent").toString();
			 String content = objTemp.get("content").toString();
			 builder.append(templateContent).append(" ").append(content);
			 list.add(builder.toString());
		}
		return list;
	}
	
	
	
	 public static boolean checkEmail(String email){
	        boolean flag = false;
	        try{
	            String check = "^([a-z0-9A-Z]+[-|_|\\.]?)+[a-z0-9A-Z]@([a-z0-9A-Z]+(-[a-z0-9A-Z]+)?\\.)+[a-zA-Z]{2,4}$";	    
	            Pattern regex = Pattern.compile(check);
	            Matcher matcher = regex.matcher(email);
	            flag = matcher.matches();
	            }catch(Exception e){
	                flag = false;
	            }
	        return flag;
	    }
	
	
	

}
