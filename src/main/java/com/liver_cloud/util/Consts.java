package com.liver_cloud.util;

public class Consts {
	
	public static String Doctor = "doctor";
	public static String Patient = "patient";
	
	/**
	 * 不是助手
	 */
	public static final String AssistantTypeNo="1";
	/**
	 * 是助手但未绑定医生
	 */
	public static final String AssistantTypeUnbind="2";
	/**
	 * 是自己的助手
	 */
	public static final String AssistantTypeSelf="3";
	/**
	 * 是别人的助手
	 */
	public static final String AssistantTypeOther="4";

}
