//重新计算html的font-size
(function () {
    function setFontSize() {
        var html = document.documentElement,
            viewWidth = html.clientWidth / 7.5;
        html.style.fontSize = viewWidth + 'px';
    }
    document.addEventListener('DOMContentLoaded', setFontSize, false);
    window.onresize = setFontSize;
}());