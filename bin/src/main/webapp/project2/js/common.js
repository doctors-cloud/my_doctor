//线上
const baseUrl = 'http://camp.liver-cloud.com/my_doctor';
//const baseUrl = 'http://10.0.1.2:8080/my_doctor';
//线下
// const baseUrl = 'http://10.0.1.73:8080/my_doctor';

(function () {

    // 设置根font-size
    var html = document.documentElement,
        viewWidth = html.clientWidth / 7.5;
    html.style.fontSize = viewWidth + 'px';
}());

//判断客户端类型
var u = navigator.userAgent;
var isAndroid = u.indexOf('Android') > -1 || u.indexOf('Adr') > -1;
var isiOS = !!u.match(/\(i[^;]+;( U;)? CPU.+Mac OS X/);
var a = document.createElement('a');
a.setAttribute('id', 'openApp');
document.querySelector('body').appendChild(a);

if (isAndroid) {
    a.href = 'http://camp.liver-cloud.com/my_doctor/project2/androiddownload.html';
    a.innerHTML = '前往下载页';
    console.log('这里是安卓');
}

if (isiOS) {
    a.innerHTML = '打开Liver-Care';
    a.href = 'https://itunes.apple.com/cn/app/livercare-fl-zhuan-ye-zhi/id1183655170?mt=8';
    document.getElementById('openApp').onclick = function (e) {
        var ifr = document.createElement('iframe');
        ifr.src = 'com.baidu.tieba://';
        ifr.style.display = 'none';
        document.body.appendChild(ifr);
        window.setTimeout(function () {
            document.body.removeChild(ifr);
        }, 3000)
    };
}

if (!isiOS && !isAndroid) {
    a.innerHTML = '请前往下载Liver-Care';
    a.href = 'http://camp.liver-cloud.com/my_doctor/project2/download.html';

}


//获取用户参数
function GetQueryString(url, name) {
    var pattern = new RegExp("[?&]" + name + "\=([^&]+)", "g");
    var matcher = pattern.exec(url);
    var items = null;
    if (null != matcher) {
        try {
            items = decodeURIComponent(decodeURIComponent(matcher[1]));
        } catch (e) {
            try {
                items = decodeURIComponent(matcher[1]);
            } catch (e) {
                items = matcher[1];
            }
        }
    }
    return items;
}

