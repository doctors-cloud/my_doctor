//线上
const baseUrl = 'http://camp.liver-cloud.com/my_doctor';
//线下
// const baseUrl = 'http://10.0.1.73:8080/my_doctor';


//获取用户ID
var href = location.href;

//转化时间
function transition(time) {
    var currentTime = new Date(time);
    var year = currentTime.getFullYear(),
        month = currentTime.getMonth() + 1,
        day = currentTime.getDate();

    return year + '年' + month + '月' + day + '日';
}